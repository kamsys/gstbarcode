package com.gst.wms.enumerations;

public enum RecvType {
	
	NORMAL("발주입고"),
	NOTREGISTER("부외재고입고"),
	NOPO("무상입고"),
	RETURN("환입"),
	DEFECTIVE("반품"),
	CROSSDOCK("크로스도킹");
	
	private String recvType;
	
	private RecvType(){}
	private RecvType(String type){
		this.recvType = type;
	}
	
	public String getRecvType(){
		return recvType;
	}
	
}
