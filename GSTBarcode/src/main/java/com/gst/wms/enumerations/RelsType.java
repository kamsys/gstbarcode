package com.gst.wms.enumerations;

public enum RelsType {
	
	FPROD("생산출고"),
	SERVICE("서비스출고"),
	PROD("제품출하"),
	RESCH("연구출고"),
	TERMN("폐기출고"),
	OTHER("타계정출고");
	
	private String recvType;
	
	private RelsType(){}
	private RelsType(String type){
		this.recvType = type;
	}
	
	public String getRecvType(){
		return recvType;
	}
	
}
