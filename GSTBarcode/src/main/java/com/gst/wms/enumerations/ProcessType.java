package com.gst.wms.enumerations;

public enum ProcessType {
	
	RELEASE("출고"),
	RECV("입고"),
	INVENMOVE("자재이동"),
	CHGECAT("품목전환");
	
	private String processType;
	
	private ProcessType(){}
	
	private ProcessType(String type){
		this.processType = type;
	}
	
	public String getProcessType(){
		return this.processType;
	}
}
