package com.gst.wms.enumerations;

public enum PurchaseOrderType {
	
	OP("일반재자"),
	OF("외자"),
	OO("외주");
	
	private String poType;
	
	private PurchaseOrderType(){}
	
	private PurchaseOrderType(String type){
		this.poType = type;
	}
	
	public String getPurchaseOrderType(){
		return this.poType;
	}
}
