/**
 * 
 */
package com.gst.wms.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import scala.collection.concurrent.Debug;

/**
 * @FileName  : Scheduler.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 3. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 * 		주기적으로 반복실행되는 프로세스를 정의한다. 
 * 		@Scheduled(cron = "초 분 시(24시) 일 월 요일")
 * 		@Scheduled(cron = "* * * * * *") * = 모두 또는 항상 
 * 		@Scheduled(fixedDelay = 3600000) // 1000 * 60 * 60, 즉 1시간마다 실행한다.
 */

@Component
public class Scheduler {
	
	private static final Logger logger = LoggerFactory.getLogger(Scheduler.class);
	
	@Scheduled(cron="0 0 13 * * *")
	public void scheduleFinishAfterOneWeek() {
		Debug.log("Scheduler");
	}

}
