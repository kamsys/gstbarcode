package com.gst.wms.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.Socket;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;

import com.gst.wms.dto.PrintData;

import net.sf.json.JSONException;

public class PrintBarcode {
	
	private static final Logger logger = LoggerFactory.getLogger(PrintBarcode.class);
	
	public boolean barcodeBig(PrintData data)
	{
		
		logger.debug("프린터 호출함");
		
		try{
			String commands = "^XA"
							+ "^LT0^MN" + data.getPrintSystem() + "^MT" + data.getPrintWay()
							+ "^LH0,0^JMA^PR" + data.getPrintSpeed() + "~SD" + data.getPrintConsist() + "^JUS^CI0"
							+ "^MMT"
							+ "^PW508"
							+ "^LL0285"
							+ "^LS0"
							+ "^FO0,0^GFA,01536,01536,00024,:Z64:" + data.getItemDesc()
							+ "^FO0,224^GFA,02048,02048,00032,:Z64:" + data.getPrintDate()
							+ "^FO352,0^GFA,01280,01280,00020,:Z64:" + data.getItemTitle()
							+ "^FO1,1^GB505,281,4^FS"
							+ "^BY4,3,146^FT105,200^BCN,,Y,N"
							+ "^FD>;" + data.getItemNo() + "^FS"
							+ "^PQ" + data.getPrintCount() + ",0,1,Y"
							+ "^XZ";
			/*= "^XA"
			+ "^LT0^MNN^MTD^LH0,0^JMA^PR3~SD30^JUS^CI0"
			+ "^MMT"
			+ "^PW527"
			+ "^LL0285"
			+ "^LS0"
			+ "^FO32,192^GFA,03840,03840,00060,:Z64:"
			+ "eJztVEGu0zAUfJYX3sUXiOIrsAxSaK6UvyIS0W9YsYMLcBj/eyDkG+AdWUQ1M3bSFj4S6po8tbXjet543htH5IgjjjjiiCNKGP84JKU0c6Ln60oZz1HcilElLwaT86vkxHqVUjQZu4j02IuvCq10NieNeJ51CnhIaQHJKp3wkxmBzbwK+VcTZaCGRgYKsTLJZLx5WXiGtBivojwLVnelYN+wAeRvsCqVBI3FCgxBBSut8E9QYRXY9ar0R1atvwuSTuR1olRUwDZSYxKd1MI/xcwO4jMNANigfNarP4sKIBhIB5aCbbAWMOy8JxRlVldeHIRYY4gVYhuyqFzbGkOs80ogL5S+n/W3Xa+VzFvhN5T0SC6lgEVoV04Cmo/4+TDrrztvU1Q75EE55ecqNfo5uYwdSdihAkXvJ3A9eZOxZJz6XKsGGfqZVZ/f+UXcJXuAZL4Vu02/YMPbYOsNq0PBUpm5oIbW92jNOVusj9TUEZtNxG4+RWBhq3QR50uXWybuUVUdTtC3FJOvNFHRm1LQC/aModqxl63LIwUpGij24thWYhvqvOmN0D+Odc1C1Hjc3I0j7XUFrw656MDaWW51nuHktm1rbuxuNwN2pWq2BwCYJid13FJfeemroZs2XrGp+BmeAiAbqAGvJw8JB/b7yotTjKdx57XlFqPKdfnSv44DsY52am68qODg4s67dRn1onc7hXtTcVpAjdqu08ZLy9twp5d1RkLLe6Si9obXh9cEarUv17j4mVUYTLjn9Zm7kmf0aLRw0ojsSGjnRaOtxi87L/hG4//gdYT3qbyo4Of9faX5XmH7il7sK9h7vSVJkr9GNm34beme99F4xftQdP/ecsQR/1n8AqSYM4I=:A6DD"
			+ "^FO320,0^GFA,01536,01536,00024,:Z64:"
			+ "eJztkjFyhDAMRT8RYzUam9IFRY6wpYudTI7CEXyEPQFn4igcwSXFzhLJFDhJly4zSAXM9/O3/AG46qp/WN3WFeTj/VMb4JUWgF70xEQv01dtwKdQdJVZkKnyizYQo+h+x+xRqk4rGZ/SPalOunLoYQnGD2Mc1Y1mwvSG0z/FJEBPPSG7Zh6Mg5irp8fkGl4P8DauKM9bww83473xoZkf75WXynveN+N5L12pfFI+eybVlQ/6OPyz8aB2nsov5g96nP5ifIdfvNj8rDfGNz5543tNDrlv+JvUfOAdJk9NPhKjmoGd5uNOPsZR9bDTrPeaT/5DD6j6E7lr8uGVS41x+/kv1M921V/qC1PjTZ0=:7491"
			+ "^FO0,0^GFA,01792,01792,00028,:Z64:"
			+ "eJztksFqwzAMhuUZ4sJGcg3MnfsI2s2DQV6lsBfwbjsMqrAXc9/Ej9BjYDDNSuK0gd52XHUw2J9/Sb9sgFvc4t/HgSNAO2/UcAZNkrUbCjM/coFjsIrcAFUFh4XV9cgS1pr8K1Q17LqSyhrKjAK6vDgwFoIrzOuReXxSlCqoHgB9yRlUbkxFDFkXjfQZ2sJ2ECfd/RfFu/HE+5Uus+2xj9PJUm/RtceexhNcWJhYCFZ0ivl09reHxCz+Nm9EoJmHbrioJyyife+PDErGdqGTnAnb554YZAd+VS/mei999pfvnnVznzvcPFLU0DEB09pfQLvtp7loPpV5qnGeiG1DSQMq0vRR3kEVf4b2RtIYsIXpWbfJrMnlqV56cQ4mlnWf3Vo3/QmZpyb3LfWqxd/lXzJJ+jQxwNXI/jSn60zsHuJ1dos/xC/2DYXL:BF92"
			+ "^BY6,3,104^FT31,163^BCN,,Y,N"
			+ "^FD>;11003103^FS"
			+ "^FO2,1^GB523,283,4^FS"
			+ "^PQ3,10,1,Y"
			+ "^XZ";*/
			//^XA 코드 시작
			//~TA 문자의 개수가 3미안이면, 명령은 무시된다.
			//~JS 백피드 명령 시퀀스를 제어!?
			//^LT 라벨 탑을 지정 (머릿말효과)
			//^MN 미디어 추적
			//^MT 미디어 타입
			//^PO 라벨 180도 회전
			//^PM 라벨 거울효과 (좌우반전)
			//^LH 라벨 시작점 지정
			//^JM 라벨 포맷크기를 두배로 지정, ^FS전에 실행되어야한다.
			//^PR 프린트 속도
			//~SD 프린트 농도
			//^JU 프린트 설정
			//^LR 반전효과 지정
			//^CI 글자 인코딩
			//^MM 프린트 모드 (여백 설정)
			//^PW 글자 높이
			//^LL 글자 길이
			//^LS 프린트 버전 호환
			//^FO 라벨 위치 (인쇄 시작점)
			//^GF 그래픽효과
			//^GB 박스 또는 선 만들기
			//^BY 바코드의 바의 폭 과 높이 를 설정
			//^FT ^LH에 의해 지정 , 포지션 설정
			//^BC 바코드 형식( CODE 128 형식)
			//^FD 문자열 정의 , ^FS 와 쌍을 이룬다. (예 : ^FD 문자열 ^FS)
			//^FS 문자열 정의 , ^FD 와 쌍을 이룬다. (예 : ^FD 문자열 ^FS)
			//^PQ 인쇄 횟수
			//^XZ 코드 끝
			
			String serverIp = data.getIp();
			
            // 소켓을 생성하여 연결을 요청한다.
            System.out.println("서버에 연결중입니다. 서버IP : " + serverIp);
            Socket socket = new Socket(serverIp, 9100);
            
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeBytes(commands);
            dos.flush();
            
            // 소켓의 입력스트림을 얻는다.
            //InputStream in = socket.getInputStream();
            //DataInputStream dis = new DataInputStream(in);
            
            // 소켓으로부터 받은 데이터를 출력한다.
            System.out.println("서버로부터 받은 메세지 : ");
            System.out.println("연결을 종료합니다.");
            
            dos.close();
            //dis.close();
            socket.close();
            System.out.println("연결이 종료되었습니다.");
			
		}catch (Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		return true;
		
	}
	
	public boolean barcodeMiddle(PrintData data)
	{
		
		logger.debug("프린터 호출함");
		
		try{
			String commands = "^XA"
							+ "^LT0^MNW^MTD^LH0,0^JMA^PR3~SD30^JUS^CI0"
							+ "^MMT"
							+ "^PW285"
							+ "^LL0162"
							+ "^LS0"
							+ "^FO0,0^GFA,01280,01280,00020,:Z64:" + data.getItemDesc()
							+ "^FO128,0^GFA,01280,01280,00020,:Z64:" + data.getPrintDate()
							+ "^FO0,96^GFA,01536,01536,00024,:Z64:" + data.getItemTitle()
							+ "^FO1,1^GB282,160,4^FS"
							+ "^BY2,3,61^FT42,102^BCN,,Y,N"
							+ "^FD>;" + data.getItemNo() + "^FS"
							+ "^PQ" + data.getPrintCount() + ",0,1,Y"
							+ "^XZ";
			//^XA 코드 시작
			//~TA 문자의 개수가 3미안이면, 명령은 무시된다.
			//~JS 백피드 명령 시퀀스를 제어!?
			//^LT 라벨 탑을 지정 (머릿말효과)
			//^MN 미디어 추적
			//^MT 미디어 타입
			//^PO 라벨 180도 회전
			//^PM 라벨 거울효과 (좌우반전)
			//^LH 라벨 시작점 지정
			//^JM 라벨 포맷크기를 두배로 지정, ^FS전에 실행되어야한다.
			//^PR 프린트 속도
			//~SD 프린트 농도
			//^JU 프린트 설정
			//^LR 반전효과 지정
			//^CI 글자 인코딩
			//^MM 프린트 모드 (여백 설정)
			//^PW 글자 높이
			//^LL 글자 길이
			//^LS 프린트 버전 호환
			//^FO 라벨 위치 (인쇄 시작점)
			//^GF 그래픽효과
			//^GB 박스 또는 선 만들기
			//^BY 바코드의 바의 폭 과 높이 를 설정
			//^FT ^LH에 의해 지정 , 포지션 설정
			//^BC 바코드 형식( CODE 128 형식)
			//^FD 문자열 정의 , ^FS 와 쌍을 이룬다. (예 : ^FD 문자열 ^FS)
			//^FS 문자열 정의 , ^FD 와 쌍을 이룬다. (예 : ^FD 문자열 ^FS)
			//^PQ 인쇄 횟수
			//^XZ 코드 끝
			
			String serverIp = data.getIp();
			
            // 소켓을 생성하여 연결을 요청한다.
            System.out.println("서버에 연결중입니다. 서버IP : " + serverIp);
            Socket socket = new Socket(serverIp, 9100);
            
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeBytes(commands);
            dos.flush();
            
            // 소켓의 입력스트림을 얻는다.
            InputStream in = socket.getInputStream();
            DataInputStream dis = new DataInputStream(in);
            
            // 소켓으로부터 받은 데이터를 출력한다.
            System.out.println("서버로부터 받은 메세지 : " + dis.readUTF());
            System.out.println("연결을 종료합니다.");
            
            dos.close();
            dis.close();
            socket.close();
            System.out.println("연결이 종료되었습니다.");
			
		}catch (Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		return true;
		
	}
	
	public boolean barcodeSmall(PrintData data)
	{
		
		logger.debug("프린터 호출함");
		
		try{
			String commands = "^XA"
							+ "^LT0^MNW^MTD^LH0,0^JMA^PR3~SD30^JUS^CI0"
							+ "^MMT"
							+ "^PW285"
							+ "^LL0121"
							+ "^LS0"
							+ "^FO0,0^GFA,00384,00384,00012,:Z64:" + data.getItemDesc()
							+ "^FO192,0^GFA,00384,00384,00012,:Z64:" + data.getPrintDate()
							+ "^FO0,64^GFA,01280,01280,00020,:Z64:" + data.getItemTitle()
							+ "^FO1,1^GB283,120,4^FS"
							+ "^BY2,3,36^FT40,64^BCN,,Y,N"
							+ "^FD>;" + data.getItemNo() + "^FS"
							+ "^PQ" + data.getPrintCount() + ",0,1,Y"
							+ "^XZ";
			//^XA 코드 시작
			//~TA 문자의 개수가 3미안이면, 명령은 무시된다.
			//~JS 백피드 명령 시퀀스를 제어!?
			//^LT 라벨 탑을 지정 (머릿말효과)
			//^MN 미디어 추적
			//^MT 미디어 타입
			//^PO 라벨 180도 회전
			//^PM 라벨 거울효과 (좌우반전)
			//^LH 라벨 시작점 지정
			//^JM 라벨 포맷크기를 두배로 지정, ^FS전에 실행되어야한다.
			//^PR 프린트 속도
			//~SD 프린트 농도
			//^JU 프린트 설정
			//^LR 반전효과 지정
			//^CI 글자 인코딩
			//^MM 프린트 모드 (여백 설정)
			//^PW 글자 높이
			//^LL 글자 길이
			//^LS 프린트 버전 호환
			//^FO 라벨 위치 (인쇄 시작점)
			//^GF 그래픽효과
			//^GB 박스 또는 선 만들기
			//^BY 바코드의 바의 폭 과 높이 를 설정
			//^FT ^LH에 의해 지정 , 포지션 설정
			//^BC 바코드 형식( CODE 128 형식)
			//^FD 문자열 정의 , ^FS 와 쌍을 이룬다. (예 : ^FD 문자열 ^FS)
			//^FS 문자열 정의 , ^FD 와 쌍을 이룬다. (예 : ^FD 문자열 ^FS)
			//^PQ 인쇄 횟수
			//^XZ 코드 끝
			
			String serverIp = data.getIp();
			
            // 소켓을 생성하여 연결을 요청한다.
            System.out.println("서버에 연결중입니다. 서버IP : " + serverIp);
            Socket socket = new Socket(serverIp, 9100);
            
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeBytes(commands);
            dos.flush();
            
            // 소켓의 입력스트림을 얻는다.
            InputStream in = socket.getInputStream();
            DataInputStream dis = new DataInputStream(in);
            
            // 소켓으로부터 받은 데이터를 출력한다.
            System.out.println("서버로부터 받은 메세지 : " + dis.readUTF());
            System.out.println("연결을 종료합니다.");
            
            dos.close();
            dis.close();
            socket.close();
            System.out.println("연결이 종료되었습니다.");
			
		}catch (Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		return true;
		
	}
	
}
