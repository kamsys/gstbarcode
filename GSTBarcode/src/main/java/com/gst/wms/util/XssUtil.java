package com.gst.wms.util;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 *<pre>
 *  Class Name  : XssUtil.java
 *  Description : XSS 처리 (Cross Site Scripting에 의한 공격을 막기위한 필터 ) 
 *   
 *</pre>
 */
public class XssUtil {

    /**
     * HtmlSpecialCharacter가 포함되어 있는 경우 엔티티로 변환.
     * 
     * @param str
     * @return
     */
    public static String convertXSS(String str) {

        String contents = str;

        if (StringUtils.isNotEmpty(contents)) {

            //" stYle="x:expre/**/ssion(alert('xss test'))

            contents = Pattern.compile("\\\\", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("");
            contents = Pattern.compile("%5C", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("");
            contents = Pattern.compile("%255C", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("");

            // 적용제외
            contents = Pattern.compile("(?:/\\*(?:.|\\s)*?\\*/\n{0,1})", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("");
            //contents = Pattern.compile("//", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("");
            contents = Pattern.compile("(<[^>]*) on([A-Z]+)[\t\n\r ]*=([^>]*)>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("$1 Xon$2=$3>");
            contents = Pattern.compile("<SCRIPT[\t\n\r ]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("</SCRIPT[\t\n\r ]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern
                    .compile( //
                            "<[^>]+((j|&#(106|x6A)[;]*)[\t\n\r]*(a|&#(97|x61)[;]*)[\t\n\r ]*(v|&#(118|x76)[;]*)[\t\n\r]*(a|&#(97|x61)[;]*)|v[\t\n\r]*b)[\t\n\r]*(s|&#(115|x73)[;]*)[\t\n\r]*(c|&#(99|x63)[;]*)[\t\n\r]*(r|&#(114|x72)[;]*)[\t\n\r]*(i|&#(105|x69)[;]*)[\t\n\r]*(p|&#(112|x70)[;]*)[\t\n\r]*(t|&#(116|x74)[;]*)[\t\n\r]*(:|&#(58|x3A)[;]*)[^>]+>" //
                            , Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");

            contents = Pattern.compile("<OBJECT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;object&gt;");
            contents = Pattern.compile("</OBJECT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/object&gt;");
            contents = Pattern.compile("<FORM[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;form&gt;");
            contents = Pattern.compile("</FORM[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/form&gt;");
            contents = Pattern.compile("<BASE[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;base&gt;");
            contents = Pattern.compile("</BASE[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/base&gt;");
            contents = Pattern.compile("<META[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;meta&gt;");
            contents = Pattern.compile("</META[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/meta&gt;");
            contents = Pattern.compile("<BASEFONT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;base&gt;");
            contents = Pattern.compile("</BASEFONT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/base&gt;");
            contents = Pattern.compile("<NOFRAMES[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;frame&gt;");
            contents = Pattern.compile("</NOFRAMES[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/frame&gt;");
            contents = Pattern.compile("<FRAMESET[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;frame&gt;");
            contents = Pattern.compile("</FRAMESET[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/frame&gt;");
            contents = Pattern.compile("<BGSOUND[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;bgsound&gt;");
            contents = Pattern.compile("</BGSOUND[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/bgsound&gt;");
            contents = Pattern.compile("<NOSCRIPT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("</NOSCRIPT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern.compile("<APPLET[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;applet&gt;");
            contents = Pattern.compile("</APPLET[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/applet&gt;");
            contents = Pattern.compile("<MARGUEE[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;marquee&gt;");
            contents = Pattern.compile("</MARGUEE[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/marquee&gt;");
            contents = Pattern.compile("<LINK[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;link&gt;");
            contents = Pattern.compile("</LINK[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/link&gt;");
            contents = Pattern.compile("<BLINK[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;blnk&gt;");
            contents = Pattern.compile("</BLINK[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/blnk&gt;");
            contents = Pattern.compile("<LAYER[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;layer&gt;");
            contents = Pattern.compile("</LAYER[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/layer&gt;");
            contents = Pattern.compile("<ILAYER[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;layer&gt;");
            contents = Pattern.compile("</ILAYER[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/layer&gt;");
            contents = Pattern.compile("<IFRAME[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;iframe&gt;");
            contents = Pattern.compile("</IFRAME[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/iframe&gt;");
            contents = Pattern.compile("<HTML[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;html&gt;");
            contents = Pattern.compile("</HTML[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/html&gt;");
            contents = Pattern.compile("<TEXTAREA[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;textarea&gt;");
            contents = Pattern.compile("</TEXTAREA[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/textarea&gt;");
            contents = Pattern.compile("<INPUT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;input&gt;");
            contents = Pattern.compile("</INPUT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/input&gt;");
            contents = Pattern.compile("<OPTION[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;input&gt;");
            contents = Pattern.compile("</OPTION[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/input&gt;");
            contents = Pattern.compile("<BUTTON[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;button&gt;");
            contents = Pattern.compile("</BUTTON[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/button&gt;");
            contents = Pattern.compile("<EXPRESSION[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;Exprssion&gt;");
            contents = Pattern.compile("</EXPRESSION[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/Exprssion&gt;");
            contents = Pattern.compile("<SCRIPT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("</SCRIPT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern.compile("<ABSOLUTE[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;absolute&gt;");
            contents = Pattern.compile("</ABSOLUTE[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/absolute&gt;");
            contents = Pattern.compile("<BEBAVIOR[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;behavior&gt;");
            contents = Pattern.compile("</BEBAVIOR[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/behavior&gt;");
            contents = Pattern.compile("<CONTENT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;content&gt;");
            contents = Pattern.compile("</CONTENT[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/content&gt;");
            contents = Pattern.compile("<FIXED[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;fixed&gt;");
            contents = Pattern.compile("</FIXED[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/fixed&gt;");
            contents = Pattern.compile("<INCLUDESOURCE[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;includeSoure&gt;");
            contents = Pattern.compile("</INCLUDESOURCE[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/includeSoure&gt;");
            contents = Pattern.compile("<MOZBINDING[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;mozbinding&gt;");
            contents = Pattern.compile("</MOZBINDING[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/mozbinding&gt;");

            contents = Pattern.compile("[%3C]SCRIPT[\t\n\r ]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("[%3C]/SCRIPT[\t\n\r ]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern
                    .compile(
                            "[%3C][^[%3E]]+((j|&#(106|x6A)[;]*)[\t\n\r]*(a|&#(97|x61)[;]*)[\t\n\r ]*(v|&#(118|x76)[;]*)[\t\n\r]*(a|&#(97|x61)[;]*)|v[\t\n\r]*b)[\t\n\r]*(s|&#(115|x73)[;]*)[\t\n\r]*(c|&#(99|x63)[;]*)[\t\n\r]*(r|&#(114|x72)[;]*)[\t\n\r]*(i|&#(105|x69)[;]*)[\t\n\r]*(p|&#(112|x70)[;]*)[\t\n\r]*(t|&#(116|x74)[;]*)[\t\n\r]*(:|&#(58|x3A)[;]*)[^>]+>",
                            Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");

            contents = Pattern.compile("[%3C]OBJECT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;object&gt;");
            contents = Pattern.compile("[%3C]/OBJECT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/object&gt;");
            contents = Pattern.compile("[%3C]FORM[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;form&gt;");
            contents = Pattern.compile("[%3C]/FORM[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/form&gt;");
            contents = Pattern.compile("[%3C]BASE[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;base&gt;");
            contents = Pattern.compile("[%3C]/BASE[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/base&gt;");
            contents = Pattern.compile("[%3C]META[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;meta&gt;");
            contents = Pattern.compile("[%3C]/META[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/meta&gt;");
            contents = Pattern.compile("[%3C]BASEFONT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;base&gt;");
            contents = Pattern.compile("[%3C]/BASEFONT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/base&gt;");
            contents = Pattern.compile("[%3C]NOFRAMES[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;frame&gt;");
            contents = Pattern.compile("[%3C]/NOFRAMES[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/frame&gt;");
            contents = Pattern.compile("[%3C]FRAMESET[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;frame&gt;");
            contents = Pattern.compile("[%3C]/FRAMESET[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/frame&gt;");
            contents = Pattern.compile("[%3C]BGSOUND[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;bgsound&gt;");
            contents = Pattern.compile("[%3C]/BGSOUND[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/bgsound&gt;");
            contents = Pattern.compile("[%3C]NOSCRIPT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("[%3C]/NOSCRIPT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern.compile("[%3C]APPLET[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;applet&gt;");
            contents = Pattern.compile("[%3C]/APPLET[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/applet&gt;");
            contents = Pattern.compile("[%3C]MARGUEE[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;marquee&gt;");
            contents = Pattern.compile("[%3C]/MARGUEE[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/marquee&gt;");
            contents = Pattern.compile("[%3C]LINK[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;link&gt;");
            contents = Pattern.compile("[%3C]/LINK[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/link&gt;");
            contents = Pattern.compile("[%3C]BLINK[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;blnk&gt;");
            contents = Pattern.compile("[%3C]/BLINK[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/blnk&gt;");
            contents = Pattern.compile("[%3C]LAYER[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;layer&gt;");
            contents = Pattern.compile("[%3C]/LAYER[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/layer&gt;");
            contents = Pattern.compile("[%3C]ILAYER[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;layer&gt;");
            contents = Pattern.compile("[%3C]/ILAYER[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/layer&gt;");
            contents = Pattern.compile("[%3C]IFRAME[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;iframe&gt;");
            contents = Pattern.compile("[%3C]/IFRAME[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/iframe&gt;");
            contents = Pattern.compile("[%3C]HTML[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;html&gt;");
            contents = Pattern.compile("[%3C]/HTML[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/html&gt;");
            contents = Pattern.compile("[%3C]TEXTAREA[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;textarea&gt;");
            contents = Pattern.compile("[%3C]/TEXTAREA[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/textarea&gt;");
            contents = Pattern.compile("[%3C]INPUT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;input&gt;");
            contents = Pattern.compile("[%3C]/INPUT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/input&gt;");
            contents = Pattern.compile("[%3C]OPTION[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;input&gt;");
            contents = Pattern.compile("[%3C]/OPTION[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/input&gt;");
            contents = Pattern.compile("[%3C]BUTTON[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;button&gt;");
            contents = Pattern.compile("[%3C]/BUTTON[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/button&gt;");
            contents = Pattern.compile("[%3C]EXPRESSION[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;Exprssion&gt;");
            contents = Pattern.compile("[%3C]/EXPRESSION[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/Exprssion&gt;");
            contents = Pattern.compile("[%3C]SCRIPT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("[%3C]/SCRIPT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern.compile("[%3C]ABSOLUTE[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;absolute&gt;");
            contents = Pattern.compile("[%3C]/ABSOLUTE[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/absolute&gt;");
            contents = Pattern.compile("[%3C]BEBAVIOR[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;behavior&gt;");
            contents = Pattern.compile("[%3C]/BEBAVIOR[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/behavior&gt;");

            //contents = Pattern.compile("[%3C]CONTENT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;content&gt;");
            contents = Pattern.compile("(%3C|%253C|%25|<)+(\r|\n|\t| )*CONTENT(\r|\n|\t| )*(%3E|%253E|%25|>)+", Pattern.CASE_INSENSITIVE).matcher(contents)
                    .replaceAll("&lt;content&gt;");
            //contents = Pattern.compile("[%3C]/CONTENT[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/content&gt;");
            contents = Pattern.compile("(%3C|%253C|%25|<)+(\r|\n|\t| )*/CONTENT(\r|\n|\t| )*(%3E|%253E|%25|>)+", Pattern.CASE_INSENSITIVE).matcher(contents)
                    .replaceAll("&lt;/content&gt;");

            contents = Pattern.compile("[%3C]FIXED[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;fixed&gt;");
            contents = Pattern.compile("[%3C]/FIXED[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/fixed&gt;");
            contents = Pattern.compile("[%3C]INCLUDESOURCE[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;includeSoure&gt;");
            contents = Pattern.compile("[%3C]/INCLUDESOURCE[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/includeSoure&gt;");
            contents = Pattern.compile("[%3C]MOZBINDING[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;mozbinding&gt;");
            contents = Pattern.compile("[%3C]/MOZBINDING[^[%3E]]*[%3E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/mozbinding&gt;");

            contents = Pattern.compile("[%353C]SCRIPT[\t\n\r ]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("[%353C]/SCRIPT[\t\n\r ]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern
                    .compile( //
                            "[%353C][^[%353E]]+((j|&#(106|x6A)[;]*)[\t\n\r]*(a|&#(97|x61)[;]*)[\t\n\r ]*(v|&#(118|x76)[;]*)[\t\n\r]*(a|&#(97|x61)[;]*)|v[\t\n\r]*b)[\t\n\r]*(s|&#(115|x73)[;]*)[\t\n\r]*(c|&#(99|x63)[;]*)[\t\n\r]*(r|&#(114|x72)[;]*)[\t\n\r]*(i|&#(105|x69)[;]*)[\t\n\r]*(p|&#(112|x70)[;]*)[\t\n\r]*(t|&#(116|x74)[;]*)[\t\n\r]*(:|&#(58|x3A)[;]*)[^>]+>" //
                            , Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");

            contents = Pattern.compile("[%353C]OBJECT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;object&gt;");
            contents = Pattern.compile("[%353C]/OBJECT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/object&gt;");
            contents = Pattern.compile("[%353C]FORM[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;form&gt;");
            contents = Pattern.compile("[%353C]/FORM[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/form&gt;");
            contents = Pattern.compile("[%353C]BASE[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;base&gt;");
            contents = Pattern.compile("[%353C]/BASE[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/base&gt;");
            contents = Pattern.compile("[%353C]META[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;meta&gt;");
            contents = Pattern.compile("[%353C]/META[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/meta&gt;");
            contents = Pattern.compile("[%353C]BASEFONT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;base&gt;");
            contents = Pattern.compile("[%353C]/BASEFONT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/base&gt;");
            contents = Pattern.compile("[%353C]NOFRAMES[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;frame&gt;");
            contents = Pattern.compile("[%353C]/NOFRAMES[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/frame&gt;");
            contents = Pattern.compile("[%353C]FRAMESET[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;frame&gt;");
            contents = Pattern.compile("[%353C]/FRAMESET[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/frame&gt;");
            contents = Pattern.compile("[%353C]BGSOUND[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;bgsound&gt;");
            contents = Pattern.compile("[%353C]/BGSOUND[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/bgsound&gt;");
            contents = Pattern.compile("[%353C]NOSCRIPT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("[%353C]/NOSCRIPT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern.compile("[%353C]APPLET[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;applet&gt;");
            contents = Pattern.compile("[%353C]/APPLET[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/applet&gt;");
            contents = Pattern.compile("[%353C]MARGUEE[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;marquee&gt;");
            contents = Pattern.compile("[%353C]/MARGUEE[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/marquee&gt;");
            contents = Pattern.compile("[%353C]LINK[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;link&gt;");
            contents = Pattern.compile("[%353C]/LINK[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/link&gt;");
            contents = Pattern.compile("[%353C]BLINK[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;blnk&gt;");
            contents = Pattern.compile("[%353C]/BLINK[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/blnk&gt;");
            contents = Pattern.compile("[%353C]LAYER[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;layer&gt;");
            contents = Pattern.compile("[%353C]/LAYER[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/layer&gt;");
            contents = Pattern.compile("[%353C]ILAYER[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;layer&gt;");
            contents = Pattern.compile("[%353C]/ILAYER[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/layer&gt;");
            contents = Pattern.compile("[%353C]IFRAME[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;iframe&gt;");
            contents = Pattern.compile("[%353C]/IFRAME[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/iframe&gt;");
            contents = Pattern.compile("[%353C]HTML[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;html&gt;");
            contents = Pattern.compile("[%353C]/HTML[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/html&gt;");
            contents = Pattern.compile("[%353C]TEXTAREA[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;textarea&gt;");
            contents = Pattern.compile("[%353C]/TEXTAREA[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/textarea&gt;");
            contents = Pattern.compile("[%353C]INPUT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;input&gt;");
            contents = Pattern.compile("[%353C]/INPUT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/input&gt;");
            contents = Pattern.compile("[%353C]OPTION[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;input&gt;");
            contents = Pattern.compile("[%353C]/OPTION[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/input&gt;");
            contents = Pattern.compile("[%353C]BUTTON[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;button&gt;");
            contents = Pattern.compile("[%353C]/BUTTON[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/button&gt;");
            contents = Pattern.compile("[%353C]EXPRESSION[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;Exprssion&gt;");
            contents = Pattern.compile("[%353C]/EXPRESSION[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/Exprssion&gt;");
            contents = Pattern.compile("[%353C]SCRIPT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;script&gt;");
            contents = Pattern.compile("[%353C]/SCRIPT[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/script&gt;");
            contents = Pattern.compile("[%353C]ABSOLUTE[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;absolute&gt;");
            contents = Pattern.compile("[%353C]/ABSOLUTE[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/absolute&gt;");
            contents = Pattern.compile("[%353C]BEBAVIOR[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;behavior&gt;");
            contents = Pattern.compile("[%353C]/BEBAVIOR[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/behavior&gt;");
            contents = Pattern.compile("[%353C]FIXED[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;fixed&gt;");
            contents = Pattern.compile("[%353C]/FIXED[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/fixed&gt;");
            contents = Pattern.compile("[%353C]INCLUDESOURCE[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;includeSoure&gt;");
            contents = Pattern.compile("[%353C]/INCLUDESOURCE[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/includeSoure&gt;");
            contents = Pattern.compile("[%353C]MOZBINDING[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;mozbinding&gt;");
            contents = Pattern.compile("[%353C]/MOZBINDING[^[%353E]]*[%353E]", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;/mozbinding&gt;");

            contents = Pattern.compile("eval\\((.*)\\)*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("");

            // 적용제외
            //contents = Pattern.compile("&#", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("");
            //contents = Pattern.compile("\\(", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("");
            //contents = Pattern.compile("([^A-Z]on([A-Z]+)[\t\n\r ]*)=", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("XoXnX$2");

            contents = Pattern.compile("<EMBED[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;EMBED&gt;");
            contents = Pattern.compile("<BODY[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;BODY&gt;");
            contents = Pattern.compile("<XSS[^>]*>", Pattern.CASE_INSENSITIVE).matcher(contents).replaceAll("&lt;XSS&gt;");

        } else {
            contents = "";
        }

        return contents;
    }

    /**
     * convertXSSHtmlEscape
     * 
     * @param 
     * @return String
     */
    public static String convertXSSHtmlEscape(String paramStr) {

        // XSS converting
        String convXssStr = convertXSS(convertXSSByCss(paramStr));

        String contents = convXssStr;

        if (StringUtils.isEmpty(contents)) {
            contents = "";
        } else {
            contents = contents.replaceAll("<", "&lt;");
            contents = contents.replaceAll(">", "&gt;");
            // remove non printable character
            contents = contents.replaceAll("[\\x0b-\\x1F\\x7F]", " ");
        }

        return contents;
    }

    /**
     * convertXSSByCss
     * 
     * @param 
     * @return String
     */
    public static String decodeXSSByCss(String paramStr) {

        String contents = paramStr;

        contents = contents.replaceAll("&amp;", "&");

        contents = contents.replaceAll("&#37", "%");
        contents = contents.replaceAll("&#59", ";");
        contents = contents.replaceAll("&#39", "'");
        contents = contents.replaceAll("&quot;", "\"");
        // url param 제외
        contents = contents.replaceAll("&#47", "/");
        contents = contents.replaceAll("&#42", "\\*");
        // delimiter 제외
        contents = contents.replaceAll("&#58", ":");

        return contents;
    }

    /**
     * convertXSSByCss
     * 
     * @param 
     * @return String
     */
    public static String convertXSSByCss(String paramStr) {

        String contents = paramStr;

        // 순차 적용
        contents = contents.replaceAll("&amp;", "&");
        contents = contents.replaceAll("&", "&amp;");

        contents = contents.replaceAll("%", "&#37");
        //contents = contents.replaceAll(";", "&#59");
        contents = contents.replaceAll("'", "&#39");
        contents = contents.replaceAll("\"", "&quot;");
        // url param 제외
        //contents = contents.replaceAll("/", "&#47");
        contents = contents.replaceAll("\\*", "&#42");
        // delimiter 제외
        //contents = contents.replaceAll(":", "&#58");

        return contents;
    }

    /**
     * convertXSSFixNull
     * 
     * @param 
     * @return String
     */
    public static String convertXSSFixNull(String string) {
        String retval = convertXSS(string);
        if (StringUtils.isEmpty(retval)) {
            return "";
        } else {
            return retval;
        }
    }

}
