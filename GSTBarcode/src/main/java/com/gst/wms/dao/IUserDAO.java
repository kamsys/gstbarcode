package com.gst.wms.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gst.wms.dto.SearchData;
import com.gst.wms.dto.User;

/**
 * @FileName  : IUserDAO.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 3. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */
@Repository(value="userdao")
public interface IUserDAO {
	
	/**
	 * @Method Name  : getUser
	 * @작성일   : 2015. 12. 2. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 사용자 목록 조회
	 * @param srchData
	 * @return
	 */
	public List<User> getUser(SearchData srchData);
	public Integer getCountUser(SearchData srchData);
}
