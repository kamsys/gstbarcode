package com.gst.wms.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gst.wms.dto.InventoryDTO;
import com.gst.wms.dto.SearchData;

/**
 * @FileName  : IIventoryDAO.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 30. 
 * @작성자      : 박준연

 * @변경이력 :
 * @프로그램 설명 :
 */
@Repository(value="inventoryDao")
public interface IInventoryDAO {
	/**
	 * @Method Name  : getInventory
	 * @작성일   : 2015. 12. 1. 
	 * @작성자   : 박준연
	 * @변경이력  :
	 * @Method 설명 : 재고실사 조회
	 * @param srchData
	 * @return
	 */
	public List<InventoryDTO> getInventory(SearchData srcData);
}
