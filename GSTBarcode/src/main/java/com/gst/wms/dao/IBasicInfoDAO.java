/**
 * 
 */
package com.gst.wms.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.gst.wms.dto.Department;
import com.gst.wms.dto.SearchData;
import com.gst.wms.dto.Supplier;

/**
 * @FileName  : IBasicInfoDAO.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 27. 
 * @작성자      : God
 * http://www.iros.go.kr/efrontservlet?cmd=EC2GetPrintApplListC#0

 * @변경이력 :
 * @프로그램 설명 :   
 */

@Repository(value="basicinfoDao")
public interface IBasicInfoDAO {

	/**
	 * @Method Name  : getDeptList
	 * @작성일   : 2015. 11. 27. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 부서목록을 조회 / 리턴한다.
	 * @param srchData
	 * @return
	 */
	public List<Department> getDeptList(SearchData srchData);
	
	/**
	 * @Method Name  : getSuppliers
	 * @작성일   : 2015. 11. 30. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 거래처 목록을 조회한다. 
	 * @param srchData
	 * @return
	 */
	public List<Supplier> getSuppliers(SearchData srchData);
}
