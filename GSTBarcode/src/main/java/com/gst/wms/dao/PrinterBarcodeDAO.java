package com.gst.wms.dao;

import org.springframework.stereotype.Repository;

/**
 * @FileName  : PrinterBarcodeDAO.java
 * @Project   : GSTBarcode
 * @Date      : 2015. 11. 25. 
 * @작성자    : min

 * @변경이력 :
 * @프로그램 설명 :
 */

@Repository(value = "printerbarcodedao")
public interface PrinterBarcodeDAO {
	
}
