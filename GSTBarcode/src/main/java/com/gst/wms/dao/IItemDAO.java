/**
 * 
 */
package com.gst.wms.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.gst.wms.dto.Item;
import com.gst.wms.dto.Location;
import com.gst.wms.dto.SearchData;

/**
 * @FileName  : IItemDAO.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 30. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Repository(value="itemDao")
public interface IItemDAO {
	
	/**
	 * @Method Name  : getItems
	 * @작성일   : 2015. 11. 30. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 품목목록을 조회한다. 
	 * @param srchData
	 * @return
	 */
	public List<Item> getItems(SearchData srchData);
	/**
	 * @Method Name  : getCountItems
	 * @작성일   : 2015. 12. 2. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 품목조회를 위해 품목목록의 총 row수를 계산한다
	 * @param srchData
	 * @return
	 */
	public int getCountItems(SearchData srchData);
	
	/**
	 * @Method Name  : getLocations
	 * @작성일   : 2015. 12. 2. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 창고코드 또는 품목번호를 이용해 장소정보를 조회한다.
	 * @param whcode
	 * @param itemno
	 * @return
	 */
	public List<Location> getLocations(@Param("whcode") String whcode, 
									  @Param("itemNo") String itemno);

}
