/**
 * 
 */
package com.gst.wms.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gst.wms.dto.PoItem;
import com.gst.wms.dto.PurchaseOrder;
import com.gst.wms.dto.SearchData;

/**
 * @FileName  : IRecvDAO.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 17. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Repository(value="recvDao")
public interface IRecvDAO {

	public List<PurchaseOrder> getPoList(SearchData srchData);
	
	public List<PoItem> getPoItems(SearchData srchData);
	
}
