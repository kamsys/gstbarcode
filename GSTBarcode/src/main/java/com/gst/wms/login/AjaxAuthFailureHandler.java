/**
 * 
 */
package com.gst.wms.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

/**
 * @FileName  : AjaxAuthFailureHandler.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 3. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

public class AjaxAuthFailureHandler extends
		SimpleUrlAuthenticationFailureHandler {
	
	private RedirectStrategy redirect = new DefaultRedirectStrategy();
	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		
		try 
		{
			String targetUrl = "/index.html#/page/ctmlogin";
	        redirect.sendRedirect(request, response, targetUrl);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

}
