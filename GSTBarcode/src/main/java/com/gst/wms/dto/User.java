/**
 * 
 */
package com.gst.wms.dto;

/**
 * @FileName  : User.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 2. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

public class User extends AbstractDTO {

	private static final long serialVersionUID = 4220461820168818967L;
	
	private String birthDate; //LS01
	private String cntctNo; //전화번호 B1PHN2
	private String deptNo; //B1V5HDPT
	private String deptName; //부서명 
	private String empNo; //B1V5HEMPNO
	private String isResign; //Y or N DL02
	private String pass;
	private String position; //직급 DL01
	private String userId; //ULUSER
	private String userName; //B1NME
	private String using; //사용여부 
	
	
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getCntctNo() {
		return cntctNo;
	}
	public void setCntctNo(String cntctNo) {
		this.cntctNo = cntctNo;
	}
	public String getDeptNo() {
		return deptNo;
	}
	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}
	public String getEmpNo() {
		return empNo;
	}
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}
	public String getIsResign() {
		return isResign;
	}
	public void setIsResign(String isResign) {
		this.isResign = isResign;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUsing() {
		return using;
	}
	public void setUsing(String using) {
		this.using = using;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
	
	
	
}
