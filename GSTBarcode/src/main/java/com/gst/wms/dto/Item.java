/**
 * 
 */
package com.gst.wms.dto;

/**
 * @FileName  : Item.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 30. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

public class Item extends AbstractDTO {

	private static final long serialVersionUID = 4220461820168818967L;
	
	private String buyUnit; //구매단위 IMUOM3
	private float convFactor; //변환계수 UMCONV
	private String glType; //GL범주 IMGLPT
	private String itemName; //품명 IMDSC1
	private String itemNo; //품번 IMLITM
	private String itemSno; //약식품번 IMITM
	private String itemSpec; //내역2 IMDSC2
	private String itemType; //재고유형 IMSTKT
	private String loc;// location
	private String lot; //lot serial number
	private Object pocode; //purchase order object
	private double serial; //item serial number
	private String standard; //serach string IMSRTX
	private float totalPrice; //unit price * qty
	private String unit; //주단위 IMUOM1
	private float unitPrice; //단가 COUNCS
	
	public String getBuyUnit() {
		return buyUnit;
	}
	public void setBuyUnit(String buyUnit) {
		this.buyUnit = buyUnit;
	}
	public float getConvFactor() {
		return convFactor;
	}
	public void setConvFactor(float convFactor) {
		this.convFactor = convFactor;
	}
	public String getGlType() {
		return glType;
	}
	public void setGlType(String glType) {
		this.glType = glType;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public String getItemSno() {
		return itemSno;
	}
	public void setItemSno(String itemSno) {
		this.itemSno = itemSno;
	}
	public String getItemSpec() {
		return itemSpec;
	}
	public void setItemSpec(String itemSpec) {
		this.itemSpec = itemSpec;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public String getLot() {
		return lot;
	}
	public void setLot(String lot) {
		this.lot = lot;
	}
	public Object getPocode() {
		return pocode;
	}
	public void setPocode(Object pocode) {
		this.pocode = pocode;
	}
	public double getSerial() {
		return serial;
	}
	public void setSerial(double serial) {
		this.serial = serial;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public float getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	
}
