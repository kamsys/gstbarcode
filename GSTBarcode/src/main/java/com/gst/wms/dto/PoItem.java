/**
 * 
 */
package com.gst.wms.dto;

import java.util.List;

/**
 * @FileName  : PoItem.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 3. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

public class PoItem {

	private static final long serialVersionUID = 4220461820168818967L;
	
	private int seq;
	
	private String delvryDate;
	private Item item;
	private Double lineNo;
	private String lineType;
	private Integer opCode;
	private String opType;
	private String orCode;
	private String orType;
	
	private int prevRecvQty;
	private int recvQty;
	
	private String sfxo;
	private String whcode;
	
	private String company;

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getDelvryDate() {
		return delvryDate;
	}

	public void setDelvryDate(String delvryDate) {
		this.delvryDate = delvryDate;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Double getLineNo() {
		return lineNo;
	}

	public void setLineNo(Double lineNo) {
		this.lineNo = lineNo;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public Integer getOpCode() {
		return opCode;
	}

	public void setOpCode(Integer opCode) {
		this.opCode = opCode;
	}

	public String getOpType() {
		return opType;
	}

	public void setOpType(String opType) {
		this.opType = opType;
	}

	public String getOrCode() {
		return orCode;
	}

	public void setOrCode(String orCode) {
		this.orCode = orCode;
	}

	public String getOrType() {
		return orType;
	}

	public void setOrType(String orType) {
		this.orType = orType;
	}

	public int getPrevRecvQty() {
		return prevRecvQty;
	}

	public void setPrevRecvQty(int prevRecvQty) {
		this.prevRecvQty = prevRecvQty;
	}

	public int getRecvQty() {
		return recvQty;
	}

	public void setRecvQty(int recvQty) {
		this.recvQty = recvQty;
	}

	public String getSfxo() {
		return sfxo;
	}

	public void setSfxo(String sfxo) {
		this.sfxo = sfxo;
	}

	public String getWhcode() {
		return whcode;
	}

	public void setWhcode(String whcode) {
		this.whcode = whcode;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	
	
}
