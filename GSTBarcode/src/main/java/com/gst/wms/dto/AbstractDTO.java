/**
 * 
 */
package com.gst.wms.dto;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @FileName  : AbstractDTO.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 30. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

public abstract class AbstractDTO implements Serializable {

	public String toString()

    {

        Class<? extends AbstractDTO> klazz = this.getClass();

        StringBuffer strBuff = new StringBuffer();
        

        Method[] methods = klazz.getDeclaredMethods();
        

        for (Method method : methods) {

            if (method.getName().startsWith("get") && method.getGenericParameterTypes().length == 0) {

                Object returnObject = null;

                try {

                    returnObject = method.invoke(this);

                } catch (IllegalArgumentException e) {

                    e.printStackTrace();

                } catch (IllegalAccessException e) {

                    e.printStackTrace();

                } catch (InvocationTargetException e) {

                    e.printStackTrace();

                }

                System.out.println(method.getName() + " - " + returnObject);

            }

        }

        return strBuff.toString();

    }

}
