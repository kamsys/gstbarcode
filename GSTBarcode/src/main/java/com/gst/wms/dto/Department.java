/**
 * 
 */
package com.gst.wms.dto;

/**
 * @FileName  : Department.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 27. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 : 부서객체 
 */

public class Department {

	private static final long serialVersionUID = 4220461820168818967L;
	
	private int seq; //순차번호 
	private String deptcode; //부서코드 
	private String deptname; //부서명  
	private String depthead; //부서장 
	private String deptheadno; //부서장명 
	private String cstdeptcode; //원가부서코드 
	private String cstdeptname; //원가부서명 
	
	
	
	/**
	 * @return the seq
	 */
	public int getSeq() {
		return seq;
	}
	/**
	 * @param seq the seq to set
	 */
	public void setSeq(int seq) {
		this.seq = seq;
	}
	/**
	 * @return the 부서코드
	 */
	public String getDeptcode() {
		return deptcode;
	}
	/**
	 * @param deptcode the 부서코드 to set
	 */
	public void setDeptcode(String deptcode) {
		this.deptcode = deptcode;
	}
	/**
	 * @return the 부서명
	 */
	public String getDeptname() {
		return deptname;
	}
	/**
	 * @param deptname the 부서명 to set
	 */
	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}
	/**
	 * @return the 부서장명
	 */
	public String getDepthead() {
		return depthead;
	}
	/**
	 * @param depthead the 부서장명 to set
	 */
	public void setDepthead(String depthead) {
		this.depthead = depthead;
	}
	/**
	 * @return the 부서장번호
	 */
	public String getDeptheadno() {
		return deptheadno;
	}
	/**
	 * @param deptheadno the 부서장번호 to set
	 */
	public void setDeptheadno(String deptheadno) {
		this.deptheadno = deptheadno;
	}
	/**
	 * @return the 원가부서코드
	 */
	public String getCstdeptcode() {
		return cstdeptcode;
	}
	/**
	 * @param cstdeptcode the 원가부서코드 to set
	 */
	public void setCstdeptcode(String cstdeptcode) {
		this.cstdeptcode = cstdeptcode;
	}
	/**
	 * @return the 원가부서명
	 */
	public String getCstdeptname() {
		return cstdeptname;
	}
	/**
	 * @param cstdeptname the 원가부서명 to set
	 */
	public void setCstdeptname(String cstdeptname) {
		this.cstdeptname = cstdeptname;
	}
	
	
	
}
