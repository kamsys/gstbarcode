package com.gst.wms.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @FileName  : SearchData.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 9. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 : 데이터 검색을 하는 경우 검색정보를 담는 객체 
 */
public class SearchData extends AbstractDTO {

	private static final long serialVersionUID = 4220461820168818967L;
	
	private long code; //po, wo, sequence etc
	private String title;
	private String content; //search content
	private String opt; // option
	private String optvalue; //value of option
	private Date finishdate;
	private Date startdate;

	private String WhCd; // 공장
	private String itemCd; // 품목코드
	private String itemNm; // 품명

	
	private Integer count;
	private Integer start;

	
	public long getCode() {
		return code;
	}
	public void setCode(long code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getFinishdate() {
		return finishdate;
	}
	public void setFinishdate(Date finishdate) {
		this.finishdate = finishdate;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public String getWhCd() {
		return WhCd;
	}
	public void setWhCd(String whCd) {
		WhCd = whCd;
	}
	public String getItemCd() {
		return itemCd;
	}
	public void setItemCd(String itemCd) {
		this.itemCd = itemCd;
	}
	public String getItemNm() {
		return itemNm;
	}
	public void setItemNm(String itemNm) {
		this.itemNm = itemNm;

	}
	public String getOpt() {
		return opt;
	}
	public void setOpt(String opt) {
		this.opt = opt;
	}
	public String getOptvalue() {
		return optvalue;
	}
	public void setOptvalue(String optvalue) {
		this.optvalue = optvalue;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
	
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public void resetProperties(){
		this.code = 0;
		this.title = null;
		this.content = null;
		this.finishdate = null;
		this.startdate = null;

		this.WhCd = null;
		this.itemCd = null;
		this.itemNm = null;

		this.opt = null;
		this.optvalue = null;

	}
	
	
}
