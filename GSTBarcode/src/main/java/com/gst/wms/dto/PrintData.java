package com.gst.wms.dto;

public class PrintData extends AbstractDTO {
	
	private String itemDesc;
	private String itemNo;
	private String itemTitle;
	private String printDate;
	private String Ip;
	private String printCount;
	private String printSpeed;
	private String printConsist;
	private String printWay;
	private String printSystem;
	
	public String getPrintSpeed() {
		return printSpeed;
	}
	public void setPrintSpeed(String printSpeed) {
		this.printSpeed = printSpeed;
	}
	public String getPrintConsist() {
		return printConsist;
	}
	public void setPrintConsist(String printConsist) {
		this.printConsist = printConsist;
	}
	public String getPrintWay() {
		return printWay;
	}
	public void setPrintWay(String printWay) {
		this.printWay = printWay;
	}
	public String getPrintSystem() {
		return printSystem;
	}
	public void setPrintSystem(String printSystem) {
		this.printSystem = printSystem;
	}
	public String getPrintCount() {
		return printCount;
	}
	public void setPrintCount(String printCount) {
		this.printCount = printCount;
	}
	public String getIp() {
		return Ip;
	}
	public void setIp(String Ip) {
		this.Ip = Ip;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public String getItemTitle() {
		return itemTitle;
	}
	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	
}
