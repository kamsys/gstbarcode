package com.gst.wms.dto;

/**
 * @FileName  : InventoryDTO.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 27. 
 * @작성자      : 박준연

 * @변경이력 :
 * @프로그램 설명 : 재고객체 
 */

public class Inventory {
	
	private String itemCd;  //품목번호
	private String itemNm; // 픔목명
	private String unit; // 단위
	private int qty; // 재고수량
	private double price; // 단가
	private String whNm; // 공장명
	private String loc; // 로케이션
	
	/**
	 * @return the 품목번호
	 */
	public String getItemCd() {
		return itemCd;
	}
	/**
	 * @param 인벤토리 the 품목번호 to set
	 */
	public void setItemCd(String itemCd) {
		this.itemCd = itemCd;
	}
	/**
	 * @return the 품목명
	 */
	public String getItemNm() {
		return itemNm;
	}
	/**
	 * @param 인벤토리 the 품목명 to set
	 */
	public void setItemNm(String itemNm) {
		this.itemNm = itemNm;
	}
	/**
	 * @return the 단위
	 */
	public String getUnit() {
		return unit;
	}
	/**
	 * @param 인벤토리 the 단위 to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	/**
	 * @return the 재고수량
	 */
	public int getQty() {
		return qty;
	}
	/**
	 * @param 인벤토리 the 재고수량 to set
	 */
	public void setQty(int qty) {
		this.qty = qty;
	}
	/**
	 * @return the 단가
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param 인벤토리 the 단가 to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the 창고명
	 */
	public String getWhNm() {
		return whNm;
	}
	/**
	 * @param 인벤토리 the 창고명 to set
	 */
	public void setWhNm(String whNm) {
		this.whNm = whNm;
	}
	/**
	 * @return the 로케이션
	 */
	public String getLoc() {
		return loc;
	}
	/**
	 * @param 인벤토리 the 로케이션 to set
	 */
	public void setLoc(String loc) {
		this.loc = loc;
	}
	
	
	
}
