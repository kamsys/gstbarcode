/**
 * 
 */
package com.gst.wms.dto;

/**
 * @FileName  : Supplier.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 30. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 : 거래처 정보 객체
 */

public class Supplier extends AbstractDTO {
	
	private static final long serialVersionUID = 4220461820168818967L;
	
	private int seq;
	private String taxno; //ABTAX
	private String suppno; //ABAN8
	private String suppaddr; //ALADD1
	private String headname; //WWMLNM
	private String comname; //ABALPH
	
	
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getTaxno() {
		return taxno;
	}
	public void setTaxno(String taxno) {
		this.taxno = taxno;
	}
	public String getSuppno() {
		return suppno;
	}
	public void setSuppno(String suppno) {
		this.suppno = suppno;
	}
	public String getSuppaddr() {
		return suppaddr;
	}
	public void setSuppaddr(String suppaddr) {
		this.suppaddr = suppaddr;
	}
	public String getHeadname() {
		return headname;
	}
	public void setHeadname(String headname) {
		this.headname = headname;
	}
	public String getComname() {
		return comname;
	}
	public void setComname(String comname) {
		this.comname = comname;
	}
	
}
