/**
 * 
 */
package com.gst.wms.dto;

import java.util.Date;
import java.util.List;

import com.gst.wms.enumerations.RecvType;

/**
 * @FileName  : PurchaseOrder.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 3. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 : 입고를 위한 발주서 또는 유사문서 
 */

public class PurchaseOrder extends AbstractDTO {

	private static final long serialVersionUID = 4220461820168818967L;
	
	private int seq;
	private String company; //
	private String addr; //
	private String chrgFax; //
	private String chrgOrder; //
	private String chrgReq; //
	private String chrgTel; //
	private List<Item> items; //
	private String note; //
	private Integer opCode; //
	private String opType; //
	private String orCode;
	private String orType;
	private String orderAddr; //
	private String orderDept; //
	private String orderHead; //
	private String orderOrg; //
	private String orderTax; //
	
	private String payCond; //
	private String payDate; //
	private Integer poDate; //발주 날짜
	
	private String suppFax; //
	private String suppHead; //
	private Integer supplier; //
	private String suppName; //
	private String suppTel; //
	
	private String whCode; //
	
	private Date recvDate;
	private RecvType recvType; //
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getChrgFax() {
		return chrgFax;
	}
	public void setChrgFax(String chrgFax) {
		this.chrgFax = chrgFax;
	}
	public String getChrgOrder() {
		return chrgOrder;
	}
	public void setChrgOrder(String chrgOrder) {
		this.chrgOrder = chrgOrder;
	}
	public String getChrgReq() {
		return chrgReq;
	}
	public void setChrgReq(String chrgReq) {
		this.chrgReq = chrgReq;
	}
	public String getChrgTel() {
		return chrgTel;
	}
	public void setChrgTel(String chrgTel) {
		this.chrgTel = chrgTel;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Integer getOpCode() {
		return opCode;
	}
	public void setOpCode(Integer opCode) {
		this.opCode = opCode;
	}
	public String getOpType() {
		return opType;
	}
	public void setOpType(String opType) {
		this.opType = opType;
	}
	public String getOrCode() {
		return orCode;
	}
	public void setOrCode(String orCode) {
		this.orCode = orCode;
	}
	public String getOrType() {
		return orType;
	}
	public void setOrType(String orType) {
		this.orType = orType;
	}
	public String getOrderAddr() {
		return orderAddr;
	}
	public void setOrderAddr(String orderAddr) {
		this.orderAddr = orderAddr;
	}
	public String getOrderDept() {
		return orderDept;
	}
	public void setOrderDept(String orderDept) {
		this.orderDept = orderDept;
	}
	public String getOrderHead() {
		return orderHead;
	}
	public void setOrderHead(String orderHead) {
		this.orderHead = orderHead;
	}
	public String getOrderOrg() {
		return orderOrg;
	}
	public void setOrderOrg(String orderOrg) {
		this.orderOrg = orderOrg;
	}
	public String getOrderTax() {
		return orderTax;
	}
	public void setOrderTax(String orderTax) {
		this.orderTax = orderTax;
	}
	public String getPayCond() {
		return payCond;
	}
	public void setPayCond(String payCond) {
		this.payCond = payCond;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public Integer getPoDate() {
		return poDate;
	}
	public void setPoDate(Integer poDate) {
		this.poDate = poDate;
	}
	public String getSuppFax() {
		return suppFax;
	}
	public void setSuppFax(String suppFax) {
		this.suppFax = suppFax;
	}
	public String getSuppHead() {
		return suppHead;
	}
	public void setSuppHead(String suppHead) {
		this.suppHead = suppHead;
	}
	public Integer getSupplier() {
		return supplier;
	}
	public void setSupplier(Integer supplier) {
		this.supplier = supplier;
	}
	public String getSuppName() {
		return suppName;
	}
	public void setSuppName(String suppName) {
		this.suppName = suppName;
	}
	public String getSuppTel() {
		return suppTel;
	}
	public void setSuppTel(String suppTel) {
		this.suppTel = suppTel;
	}
	public String getWhCode() {
		return whCode;
	}
	public void setWhCode(String whCode) {
		this.whCode = whCode;
	}
	public Date getRecvDate() {
		return recvDate;
	}
	public void setRecvDate(Date recvDate) {
		this.recvDate = recvDate;
	}
	public RecvType getRecvType() {
		return recvType;
	}
	public void setRecvType(RecvType recvType) {
		this.recvType = recvType;
	}
	
	
	
}
