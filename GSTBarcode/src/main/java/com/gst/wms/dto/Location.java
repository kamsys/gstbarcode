/**
 * 
 */
package com.gst.wms.dto;

/**
 * @FileName  : Location.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 2. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

public class Location extends AbstractDTO {

	private static final long serialVersionUID = 4220461820168818967L;
	
	private String itemNo;
	private String loc;
	private String lotsn;
	private String whcode;
	private String whname;
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public String getLotsn() {
		return lotsn;
	}
	public void setLotsn(String lotsn) {
		this.lotsn = lotsn;
	}
	public String getWhcode() {
		return whcode;
	}
	public void setWhcode(String whcode) {
		this.whcode = whcode;
	}
	public String getWhname() {
		return whname;
	}
	public void setWhname(String whname) {
		this.whname = whname;
	}
	
	
	
}
