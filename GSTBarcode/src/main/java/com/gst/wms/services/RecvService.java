/**
 * 
 */
package com.gst.wms.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gst.wms.dao.IRecvDAO;
import com.gst.wms.dto.PurchaseOrder;
import com.gst.wms.dto.SearchData;

/**
 * @FileName  : RecvService.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 17. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Service(value = "recvService")
public class RecvService {

	private static final Logger logger = LoggerFactory.getLogger(RecvService.class);
	
	@Autowired
	IRecvDAO recvDao;
	
	/**
	 * @Method Name  : getPoList
	 * @작성일   : 2015. 12. 3. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 발주서 목록 조회
	 * @param srchData
	 * @return
	 */
	public List<PurchaseOrder> getPoList(SearchData srchData)
	{
		try{
			return recvDao.getPoList(srchData);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
