/**
 * 
 */
package com.gst.wms.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gst.wms.dao.IBasicInfoDAO;
import com.gst.wms.dto.Department;
import com.gst.wms.dto.SearchData;
import com.gst.wms.dto.Supplier;

/**
 * @FileName  : BasicInfoService.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 27. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Service(value = "basicService")
public class BasicInfoService {
	
	private static final Logger logger = LoggerFactory.getLogger(BasicInfoService.class);
	
	@Autowired
	IBasicInfoDAO basicDao;
	
	/**
	 * @Method Name  : getDeptList
	 * @작성일   : 2015. 11. 27. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 부서목록을 조회 / 검색후 리턴한다. 
	 * @param srchData
	 * @return List<Department>
	 */
	public List<Department> getDeptList(SearchData srchData)
	{
		return basicDao.getDeptList(srchData);
	}
	
	
	/**
	 * @Method Name  : getgetSupplierList
	 * @작성일   : 2015. 11. 30. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 거래처목록을 조회 / 검색후 리턴한다. 
	 * @param srchData
	 * @return
	 */
	public List<Supplier> getSupplierList(SearchData srchData)
	{
		return basicDao.getSuppliers(srchData);
	}
	
}
