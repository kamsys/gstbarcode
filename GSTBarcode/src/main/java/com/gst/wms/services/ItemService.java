/**
 * 
 */
package com.gst.wms.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gst.wms.dao.IItemDAO;
import com.gst.wms.dto.Item;
import com.gst.wms.dto.Location;
import com.gst.wms.dto.SearchData;

/**
 * @FileName  : ItemService.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 30. 
 * @작성자      : God

 * @변경이력 : 품목 관련 비지니스 클래스  
 * @프로그램 설명 :
 */

@Service(value = "itemService")
public class ItemService {

	private static final Logger logger = LoggerFactory.getLogger(ItemService.class);
	
	@Autowired
	IItemDAO itemDao;
	
	/**
	 * @Method Name  : getItemList
	 * @작성일   : 2015. 11. 30. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 품목정보 검색/조회 
	 * @param srchData
	 * @return
	 */
	public HashMap<String, Object> getItemList(SearchData srchData)
	{
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("total", itemDao.getCountItems(srchData));
		result.put("list", itemDao.getItems(srchData));
		
		return result;
	}

	/**
	 * @Method Name  : getLocations
	 * @작성일   : 2015. 12. 2. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : 로케이션 정보를 창고별로 조회한다. 
	 * @param whcode
	 * @param itemNo
	 * @return
	 */
	public List<Location> getLocations(String whcode, String itemNo)
	{
		List<Location> result = new ArrayList<Location>();
		result = itemDao.getLocations(whcode, itemNo);
		return result;
	}
}
