/**
 * 
 */
package com.gst.wms.services;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gst.wms.dao.IItemDAO;
import com.gst.wms.dao.IUserDAO;
import com.gst.wms.dto.SearchData;

/**
 * @FileName  : UserService.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 2. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Service(value = "userService")
public class UserService {

private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	IUserDAO userDao;
	
	public HashMap<String, Object> getUsers(SearchData srchData)
	{
		HashMap<String, Object> result = new HashMap<String, Object>();
		try{
			int total = userDao.getCountUser(srchData);
			result.put("total", total);
			result.put("list", userDao.getUser(srchData));
			
			return result;
		}catch(Exception e){
			return null;
		}
	}
}
