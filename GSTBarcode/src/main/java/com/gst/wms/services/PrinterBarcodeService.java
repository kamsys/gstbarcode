package com.gst.wms.services;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.sf.json.JSONException;

/**
 * @FileName  : PrinterBarcode.java
 * @Project   : GSTBarcode
 * @Date      : 2015. 11. 25. 
 * @작성자    : min

 * @변경이력 :
 * @프로그램 설명 :
 */

@Service(value = "printerbarcode")
public class PrinterBarcodeService {
	
	private static final Logger logger = LoggerFactory.getLogger(PrinterBarcodeService.class);
	
	@RequestMapping(value="result", method=RequestMethod.GET)
	public String printTest(HttpServletRequest request, Model model) throws JSONException {
		
		logger.debug("프린터 호출함");
		/*
		 * 여기에서 프린팅 클래스 호출해서 구현. 
		 * */
		
		String jsp = "sample";
				
		return jsp;
		
	}
			
}