package com.gst.wms.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gst.wms.dao.IInventoryDAO;
import com.gst.wms.dto.InventoryDTO;
import com.gst.wms.dto.SearchData;;

/**
 * @FileName  : InventoryService.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 1. 
 * @작성자      : 박준연

 * @변경이력 :
 * @프로그램 설명 :
 */

@Service(value = "inventoryService")
public class InventoryService {
	private static final Logger logger = LoggerFactory.getLogger(InventoryService.class);
	
	@Autowired
	IInventoryDAO inventoryDao;
	
	/**
	 * @Method Name  : getInventory
	 * @작성일   : 2015. 12. 1. 
	 * @작성자   : 박준연
	 * @변경이력  :
	 * @Method 설명 : 재고실사 조회
	 * @param srchData
	 * @return List<InventoryDTO>
	 */
	 public List<InventoryDTO> getInventoryList(SearchData srchData)
	 {
		 
		 return inventoryDao.getInventory(srchData);
	 }
}
