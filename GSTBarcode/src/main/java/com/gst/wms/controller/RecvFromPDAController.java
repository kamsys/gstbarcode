package com.gst.wms.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @FileName  : RecvFromPDAController.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 17. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :   PDA  단말기와의 통신을 관리한다
 */

@Controller

public class RecvFromPDAController {
	
	private static final Logger logger =  LoggerFactory.getLogger(RecvFromPDAController.class);
	
	@RequestMapping(value="/api/commtest", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> commTest(@RequestBody String jsondata, HttpServletRequest request)
		throws JSONException
	{
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("result", 200);
		
		try{
			JSONObject json = new JSONObject( new JSONTokener(jsondata));
			data.put("result", 200);
			data.put("data", json.toString());
			data.put("msg", "success");
			
		}catch(Exception e){
			data.put("result", 500);
			data.put("msg", "Wrong JSON format");
		}
		
		logger.debug(jsondata);
				
		return data;
	}

	@RequestMapping(value="/api/commtestbypost", method=RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> commTestByPost(@RequestBody String jsondata, HttpServletRequest request)
			throws JSONException
			{
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("result", 200);
		
		try{
			JSONObject json = new JSONObject( new JSONTokener(jsondata));
			data.put("result", 200);
			data.put("data", json.toString());
			data.put("msg", "success");
			
		}catch(Exception e){
			data.put("result", 500);
			data.put("msg", "Wrong JSON format");
		}
		
		logger.debug(jsondata);
		
		return data;
	}
		
}
