/**
 * 
 */
package com.gst.wms.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gst.wms.dto.SearchData;
import com.gst.wms.services.ItemService;
import com.gst.wms.services.UserService;

/**
 * @FileName  : UserController.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 2. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Controller
public class UserController {

	private static final Logger logger =  LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	SearchData srchData = new SearchData();
	
	@RequestMapping(value="/getusers", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> getUsers(
			@RequestParam(value="count", required=false) Integer count, 
			@RequestParam(value="page", required=false) Integer page ,
			@RequestParam(value="opt", required=false) String opt, //검색조건
			@RequestParam(value="keyword", required=false) String keyword, //검색키워드
			HttpServletRequest request)
	{
		srchData.resetProperties();
		
		HashMap<String, Object> data = new HashMap<String, Object>();
		try{
			HashMap<String, Object> result = new HashMap<String, Object>();			
			result = userService.getUsers(srchData);
			
			data.put("result", HttpStatus.SC_OK);
			data.put("data", result.get("list"));
			data.put("total", result.get("total"));
			data.put("msg", "success");
		}catch(Exception e){
			logger.debug(e.getMessage());
		}
		
		return data;
	}
	
}
