/**
 * 
 */
package com.gst.wms.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gst.wms.dto.Item;
import com.gst.wms.dto.SearchData;
import com.gst.wms.services.BasicInfoService;
import com.gst.wms.services.ItemService;

/**
 * @FileName  : ItemController.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 30. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Controller
public class ItemController {

	private static final Logger logger =  LoggerFactory.getLogger(ItemController.class);
	
	@Autowired
	ItemService itemService;
	
	SearchData srchData = new SearchData();
	
	@RequestMapping(value="/getitems", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> getItems(
			@RequestParam(value="count", required=false) Integer count, 
			@RequestParam(value="page", required=false) Integer page ,
			@RequestParam(value="opt", required=false) String opt, //검색조건
			@RequestParam(value="keyword", required=false) String keyword, //검색키워드
			HttpServletRequest request)
	{
		srchData.resetProperties();
		
		if(count <= 0){
			srchData.setCount(25);
		}else{
			srchData.setCount(count * page);
		}

		if(page <= 0){
			srchData.setStart(0);
		}else{
			srchData.setStart((page-1) * count);
		}
		
		if(opt != null && opt.trim().length() > 0){
			if(keyword != null && keyword.trim().length() > 1){
				srchData.setOpt(opt);
				srchData.setOptvalue(keyword);
			}
		}
		

		HashMap<String, Object> data = new HashMap<String, Object>();
		try{
			HashMap<String, Object> result = new HashMap<String, Object>();			
			result = itemService.getItemList(srchData);
			
			data.put("result", 200);
			data.put("data", result.get("list"));
			data.put("total", result.get("total"));
			data.put("msg", "success");
		}catch(Exception e){
			logger.debug(e.getMessage());
		}
		
		return data;
	}
	
	@RequestMapping(value="/getlocations", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> getLocations(
			@RequestParam(value="whcode", required=false) String whcode,
			HttpServletRequest request)
	{
		HashMap<String, Object> data = new HashMap<String, Object>();
		try{
			if(whcode == null || whcode.trim().length() < 1){
				whcode = "G10"; //G10 is default
			}
			data.put("result", 200);
			data.put("data", itemService.getLocations(whcode, null));			
			data.put("msg", "success");
		}catch(Exception e){
			logger.debug(e.getMessage());
		}
		
		return data;
	}
}
