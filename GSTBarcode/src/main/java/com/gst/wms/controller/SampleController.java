/**
 * 
 */
package com.gst.wms.controller;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gst.wms.dto.PrintData;
import com.gst.wms.util.PrintBarcode;

/**
 * @FileName  : SampleController.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 26. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 * 			Invoker 상황에 따른 샘플 코드 
 */

@Controller
public class SampleController {

	private static final Logger logger =  LoggerFactory.getLogger(RecvFromPDAController.class);
	
	@RequestMapping(value="/test/calljsp", method=RequestMethod.GET)	
	public String viewJsp(HttpServletRequest request) throws JSONException
	{
		String jsp = "sample";
				
		return jsp;
	}
	
	/**
	 * @Method Name  : printTest
	 * @작성일   : 2015. 11. 26. 
	 * @작성자   : God
	 * @변경이력  :
	 * @Method 설명 : JSP에서 호출 Sample
	 * @param request
	 * @return
	 * @throws JSONException
	 */
	
	@RequestMapping(value="/test/printtest", method=RequestMethod.GET)	
	public String printTest(HttpServletRequest request, Model model) throws JSONException
	{
		logger.debug("프린터 호출함");
		/*
		 * 여기에서 프린팅 클래스 호출해서 구현. 
		 * */
		PrintBarcode barcode = new PrintBarcode();
		
		PrintData data = new PrintData();
		data.setItemDesc("itemDesc");
		data.setItemNo("itemNo");
		data.setItemTitle("itemTitle");
		data.setPrintDate("printDate");
		data.setIp("192.168.0.121");
		data.setPrintCount("1");
		data.setPrintSpeed("3");
		data.setPrintConsist("30");
		data.setPrintWay("D");
		data.setPrintSystem("M");
		
		barcode.barcodeBig(data);
		barcode.barcodeMiddle(data);
		barcode.barcodeSmall(data);
		
		String jsp = "sample";
		
		return jsp;
	}
	
}
