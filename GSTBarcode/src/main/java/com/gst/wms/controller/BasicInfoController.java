/**
 * 
 */
package com.gst.wms.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gst.wms.dto.Department;
import com.gst.wms.dto.SearchData;
import com.gst.wms.dto.Supplier;
import com.gst.wms.services.BasicInfoService;

/**
 * @FileName  : BasicInfoController.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 11. 27. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Controller
public class BasicInfoController {
	
	private static final Logger logger =  LoggerFactory.getLogger(BasicInfoController.class);
	
	@Autowired
	BasicInfoService basicService;
	
	SearchData srchData = new SearchData();
	
	@RequestMapping(value="/getdepts", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> getDepts(@RequestBody String jsondata, HttpServletRequest request)
		throws JSONException
	{
		HashMap<String, Object> data = new HashMap<String, Object>();
		JSONObject json = null;
				
		try{
			json = new JSONObject( new JSONTokener(jsondata));
			
		}catch(Exception e){
			data.put("result", 500);
			data.put("msg", "Wrong JSON format");
		}
		
		srchData.resetProperties();
		try{
			srchData.setCode( Long.parseLong( json.getString("deptcode").toUpperCase().trim()));			
		}catch(Exception e){}
		
		try{
			List<Department> list = basicService.getDeptList(srchData);
		
			
			data.put("result", 200);
			data.put("data", list);
			data.put("msg", "success");
			
			if(list == null){
				throw new Exception();
			}
		}catch(Exception e){
			data.put("result", 404);
			data.put("data", null);
			data.put("msg", "success");
			logger.debug(e.getMessage());
		}
		
		logger.debug(jsondata);
				
		return data;
	}
	
	
	@RequestMapping(value="/getsuppliers", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> getSuppliers(@RequestBody String jsondata, HttpServletRequest request)
			throws JSONException
			{
		HashMap<String, Object> data = new HashMap<String, Object>();
		JSONObject json = null;
		
		try{
			json = new JSONObject( new JSONTokener(jsondata));
			
		}catch(Exception e){
			data.put("result", 500);
			data.put("msg", "Wrong JSON format");
		}
		
		srchData.resetProperties();
		try{
			srchData.setCode( Long.parseLong( json.getString("deptcode").toUpperCase().trim()));			
		}catch(Exception e){}
		
		try{
			List<Supplier> list = basicService.getSupplierList(srchData);
			
			
			data.put("result", 200);
			data.put("data", list);
			data.put("msg", "success");
			
			if(list == null){
				throw new Exception();
			}
		}catch(Exception e){
			data.put("result", 404);
			data.put("data", null);
			data.put("msg", "success");
			logger.debug(e.getMessage());
		}
		
		logger.debug(jsondata);
		
		return data;
			}
}
