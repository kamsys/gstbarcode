package com.gst.wms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gst.wms.dto.InventoryDTO;
import com.gst.wms.dto.SearchData;
import com.gst.wms.services.InventoryService;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @FileName  : InventoryController.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 1. 
 * @작성자      : 박준연

 * @변경이력 :
 * @프로그램 설명 : 재고 콘트롤러
 */
@Controller
public class InventoryController {
	private static final Logger logger = LoggerFactory.getLogger(InventoryController.class);
	
	@Autowired
	InventoryService inventoryService;
	
	SearchData srchData = new SearchData();
	
	@RequestMapping(value="/getinventory", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> getinventory(
			@RequestParam(value="count", required=false) Integer count, 
			@RequestParam(value="page", required=false) Integer page ,
			@RequestParam(value="whCd", required=false) String whCd, //검색조건
			@RequestParam(value="itemCd", required=false) String itemCd, //검색조건
			@RequestParam(value="itemNm", required=false) String itemNm, //검색조건
			HttpServletRequest request)
		throws JSONException
	{
		srchData.resetProperties();
		HashMap<String, Object> data = new HashMap<String, Object>();
		
		
		srchData.resetProperties();
		if(count <= 0){
			srchData.setCount(25);
		}else{
			srchData.setCount(count * page);
		}

		if(page <= 0){
			srchData.setStart(0);
		}else{
			srchData.setStart((page-1) * count);
		}
		
		if(whCd != null && whCd.trim().length() > 0){
			srchData.setWhCd(whCd);	
		}
		if(whCd != null && whCd.trim().length() > 0){
			srchData.setItemCd(itemCd);	
		}
		if(whCd != null && whCd.trim().length() > 0){
			srchData.setItemNm(itemNm);		
		}
		
		try
		{
			List<InventoryDTO> result = inventoryService.getInventoryList(srchData);
			
			data.put("result", 200);
			data.put("data", result);
			data.put("msg", "success");
			
			if(result == null){
				throw new Exception();
			}
		}
		catch(Exception e)
		{
			data.put("result", 404);
			data.put("data", null);
			data.put("msg", "success");
			logger.debug(e.getMessage());
		}
		return data;
	}
}
