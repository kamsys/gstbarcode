/**
 * 
 */
package com.gst.wms.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gst.wms.dto.SearchData;
import com.gst.wms.services.RecvService;

/**
 * @FileName  : RecvController.java
 * @Project     : GSTBarcode
 * @Date         : 2015. 12. 3. 
 * @작성자      : God

 * @변경이력 :
 * @프로그램 설명 :
 */

@Controller
public class RecvController {

	private static final Logger logger =  LoggerFactory.getLogger(RecvController.class);
	
	@Autowired
	RecvService recvService;
	
	SearchData srchData = new SearchData();
	
	@RequestMapping(value="/getpolist", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> getPoList(
			@RequestParam(value="count", required=false) Integer count, 
			@RequestParam(value="page", required=false) Integer page ,
			@RequestParam(value="opt", required=false) String opt, //검색조건
			@RequestParam(value="keyword", required=false) String keyword, //검색키워드
			HttpServletRequest request)
	{
		srchData.resetProperties();
		
		HashMap<String, Object> data = new HashMap<String, Object>();
		try{
			
			data.put("result", HttpStatus.SC_OK);
			data.put("data", recvService.getPoList(srchData));
			data.put("msg", "success");
		}catch(Exception e){
			logger.debug(e.getMessage());
		}
		
		return data;
	}
}
