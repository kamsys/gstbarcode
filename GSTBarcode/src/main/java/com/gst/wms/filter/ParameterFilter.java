package com.gst.wms.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

/**
 *<pre>
 *  Class Name  : ParameterFilter.java
 *  Description : Parameter-Filter
 *  : 형태에 따라서 ParameterWrapperMultipart 와 ParameterWrapper 를 활용.
 *  
 *  Modification Information
 * 
 *   Mod Date       Modifier    Description
 *   -----------    --------    ---------------------------
 *   2013. 3. 4.    sang42      Generation
 *
 *  @author sang42
 *  @since 2013. 3. 4.
 *</pre>
 */
public class ParameterFilter implements Filter {

    public void init(FilterConfig filterConfig) {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (!excludeUrl(request)) {
            if (request instanceof MultipartHttpServletRequest) {
                DefaultMultipartHttpServletRequest mrequest = (DefaultMultipartHttpServletRequest) request;
                ParameterWrapperMultipart wrapperedRequest = new ParameterWrapperMultipart(mrequest);
                chain.doFilter(wrapperedRequest, response);
            } else if (request instanceof HttpServletRequest) {
                HttpServletRequest httpRequest = (HttpServletRequest) request;
                ParameterWrapper wrapperedRequest = new ParameterWrapper(httpRequest);
                chain.doFilter(wrapperedRequest, response);
            }
        } else {
            chain.doFilter(request, response);
        }

    }

    public void destroy() {
    }

    /**
     * 제외 URL 체크
     * 
     * @param request
     * @return
     */
    private boolean excludeUrl(ServletRequest request) {
        String uri = ((HttpServletRequest) request).getRequestURI().toString().trim();
        if (uri.startsWith("/iw-custom/web/")) {
            return true;
        } else {
            return false;
        }
    }
}
