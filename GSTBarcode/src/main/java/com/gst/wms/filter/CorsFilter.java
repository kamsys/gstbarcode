package com.gst.wms.filter;
/**
 * 0. Project  : CTM 프로젝트
 *
 * 1. FileName : CorsFilter.java
 * 2. Package : 
 * 3. Comment : 
 * 4. 작성자  : hyoungukkim
 * 5. 작성일  : Aug 21, 2015 6:08:44 PM
 * 6. 변경이력 : 
 *                    이름     : 일자          : 근거자료   : 변경내용
 *                   ------------------------------------------------------
 *                    hyoungukkim : Aug 21, 2015 :            : 신규 개발.
 */

/**
 * <pre>
 * 간략 : .
 * 상세 : .
 * 
 *   |_ CorsFilter.java
 * </pre>
 * 
 * @Company : CarrotGlobal
 * @Author  : hyoungukkim
 * @Date    : Aug 21, 2015 6:08:44 PM
 * @Version : 1.0
 */

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;
public class CorsFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            // CORS "pre-flight" request
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type");
            response.addHeader("Access-Control-Max-Age", "1800");//30 min
        }
        filterChain.doFilter(request, response);
    }
}