package com.gst.wms.filter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import com.gst.wms.util.XssUtil;

/**
 *<pre>
 *  Class Name  : ParameterWrapperMultipart.java
 *  Description : DefaultMultipartHttpServletRequest 를 활용한 Parameter-Filter
 *  
 *  Modification Information
 * 
 *   Mod Date       Modifier    Description
 *   -----------    --------    ---------------------------
 *   2013. 3. 4.    sang42      Generation
 *
 *  @author sang42
 *  @since 2013. 3. 4.
 *</pre>
 */
public class ParameterWrapperMultipart extends DefaultMultipartHttpServletRequest {

    private final Log logger = LogFactory.getLog(this.getClass());

    private final boolean isNoCheck;

    private final boolean isXssCheck;

    private final Map<String, String[]> params = new HashMap<String, String[]>();

    private final DefaultMultipartHttpServletRequest _request;

    private final Set<String> filteredParams;
    
    private final String checkUrl = "wms.gst-in.com";

    public ParameterWrapperMultipart(DefaultMultipartHttpServletRequest request) {
        super(request);
        this._request = request;

        Pattern p = Pattern.compile(request.getRequestURI());

        // ==================================================================
        // no check process
        // ==================================================================
        String filterNoCheckUrl = checkUrl;
        Matcher m = p.matcher(filterNoCheckUrl);
        isNoCheck = m.find();

        // ==================================================================
        // default XSS + HtmlEscape check process
        // ==================================================================
        String filterXssCheckUrl = checkUrl;
        Matcher m2 = p.matcher(filterXssCheckUrl);
        isXssCheck = m2.find();

        String filterNoCheckName = checkUrl;
        filteredParams = new HashSet<String>();

        if (StringUtils.isNotEmpty(filterNoCheckName)) {
            String[] _strings = StringUtils.split(filterNoCheckName, ",");
            for (String _string : _strings) {
                if (StringUtils.isNotEmpty(_string)) {
                    filteredParams.add(_string);
                }
            }
        }

        //  
        if (logger.isDebugEnabled()) {
            logger.debug("#################################################");
            logger.debug("## isNoCheck / isXssCheck : " + isNoCheck + " / " + isXssCheck);
            logger.debug("## filteredParams : " + filteredParams.toString());
            logger.debug("#################################################");
        }

        process();
    }

    /**
     * process
     * 
     * @param 
     * @return void
     */
    private void process() {

        try {

            params.putAll(_request.getParameterMap());

            super.setMultipartFiles(_request.getMultiFileMap());
            super.setMultipartParameters(_request.getParameterMap());

            for (final String key : params.keySet()) {
                final String[] vals = params.get(key);
                final String[] svals = vals;
                if (!filteredParams.contains(key)) {
                    for (int i = 0; i < vals.length; i++) {
                        replaceParam(vals, svals, i);
                    }
                }
                params.put(key, svals);
            }

        } catch (final Exception e) {
            logger.error("SanitizedRequest()" + e);
        }
    }

    /**
     * @param vals
     * @param svals
     * @param i
     */
    private void replaceParam(final String[] vals, final String[] svals, int i) {
        try {
            if (vals[i] instanceof String) {
                if (!isNoCheck) {
                    if (isXssCheck) {
                        svals[i] = XssUtil.convertXSS(vals[i]);
                    } else {
                        svals[i] = XssUtil.convertXSSHtmlEscape(vals[i]);
                    }
                }
            }
        } catch (final Exception toobad) {
            logger.debug(toobad);
        }
    }

    /* (non-Javadoc)
     * @see javax.servlet.ServletRequestWrapper#getParameterMap()
     */
    @Override
    public Map<String, String[]> getParameterMap() {
        return params;
    }

    /* 
     * (non-Javadoc)
     * @see javax.servlet.ServletRequestWrapper#getParameter(java.lang.String)
     * Request의 특정 Parameter를 String 형태로 반환한다.
     * 
     */
    @Override
    public String getParameter(final String name) {
        final String[] vals = getParameterMap().get(name);
        if (vals != null && vals.length > 0) {
            return vals[0];
        } else {
            return null;
        }

    }

    /* 
     * (non-Javadoc)
     * @see javax.servlet.ServletRequestWrapper#getParameterValues(java.lang.String)
     * Request의 특정 Parameter를 String[] 형태로 반환한다.
     * 
     */
    @Override
    public String[] getParameterValues(final String name) {
        //String[] val = getParameterMap().get(name);
        return getParameterMap().get(name);
    }
}