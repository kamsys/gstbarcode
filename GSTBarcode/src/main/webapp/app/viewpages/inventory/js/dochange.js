/**=========================================================
 * Module: inventory > change category processing
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.inventory')
        .controller('DoChangeController', DoChangeController);

    DoChangeController.$inject =['$filter', '$http', 'editableOptions', 'editableThemes','$q',
                                    '$scope', 'ngDialog'];
    function DoChangeController($filter, $http, editableOptions, editableThemes, $q, 
    								$scope, ngDialog) {
        var vm = this;
        vm.title = 'Controller';

        activate();

        ////////////////

        function activate() {
        	
        	// editable row
            // ----------------------------------- 
            vm.users = [
              {id: 1, name: 'awesome user1', itemno: 115312, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 2, name: 'awesome user1', itemno: 113232, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 3, name: 'awesome user1', itemno: 115243, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 4, name: 'awesome user1', itemno: 115454, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 5, name: 'awesome user1', itemno: 113257, unitprice: 4000, orderqty: 3, recvqty:0}
            ];

           
            vm.saveUser = function(data, id) {
              //vm.user not updated yet
              angular.extend(data, {id: id});
              console.log('Saving user: ' + id);
              // return $http.post('/saveUser', data);
            };

            // remove item
            vm.removeItem = function(index) {
              vm.users.splice(index, 1);
            };

            // add item
            vm.addItem = function() {
              vm.inserted = {
                id: vm.users.length+1,
                name: '',
                status: null,
                group: null,
                isNew: true
              };
              vm.users.push(vm.inserted);
            };

            vm.withComma = function(number){
            	console.log("number with comma");
            	return parseNumber.withComma(number);
            };
            
            vm.viewInsChange = function () {
    	        ngDialog.open({
    	          template: 'app/viewpages/inventory/partial/insChange.html',
    	          className: 'ngdialog-theme-custom',
    	          controller: 'InsChangeController',
    	          scope: $scope,
    	          cache: false
    	        })
    	    };
        }
        
    }
})();



/**=========================================================
 * Module: inventory > 품목전환 입력
  =========================================================*/

(function() {
	'use strict';
	
	angular
	.module('app.inventory')
	.controller('InsChangeController', InsChangeController);
	
	InsChangeController.$inject = ['$http', '$scope', '$log', 'ngDialog'];
	function InsChangeController($http, $scope, $log, ngDialog) {
		var vm = this;
		
		activate();
		
		////////////////
		
		function activate() {
			
			
			// Complex example
			// ----------------------------------- 
						
		}
		
	}
})();
