/**=========================================================
 * Module: inventory > 재고조회
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.inventory')
        .controller('InventoryController', InventoryController);

    InventoryController.$inject = ['$http', '$scope', '$log','ngDialog','uiGridConstants','SweetAlert'];
    function InventoryController($http, $scope, $log, ngDialog,uiGridConstants,SweetAlert) {
        var vm = this;
        var paginationOptions = {
        	    page: 1,
        	    count: 25,
        	    sort: null
        };
        
       var api = "getinventory?";
       var url = api + "count=" + paginationOptions.count + "&page=" + paginationOptions.page;
       
       var hdrTmplt = '<div class="text-center" style="margin-top: 4px;">{{col.displayName}}</div>';

       console.log($scope);
        $scope.gridOptions = {
            paginationPageSizes: [25, 50, 75],
            paginationPageSize: 25,
            useExternalPagination: true,
            useExternalSorting: true,
            columnDefs: [
              { name: '품번', field:'itemCd', enableFiltering: false, headerCellTemplate: hdrTmplt,width:'10%'},
              { name: '품명', field:'itemNm', enableFiltering: false, headerCellTemplate: hdrTmplt,width:'20%' },
              { name: '단위', field:'unit', enableFiltering: false, headerCellTemplate: hdrTmplt,width:'5%'},
              { name: '재고수량', field:'qty', enableFiltering: false, headerCellTemplate: hdrTmplt,width:'10%',cellClass: 'grid-alignR', cellFilter: 'currency:"" : 3'},
              { name: '창고', field:'whNm', enableFiltering: false, headerCellTemplate: hdrTmplt,width:'10%' },
              { name: '로케이션', field:'loc', enableFiltering: false, headerCellTemplate: hdrTmplt,width:'10%'}
              
            ],
            onRegisterApi: function(gridApi) {
            	$scope.gridApi = gridApi;
            	$scope.gridApi.core.on.sortChanged( $scope, function(grid, sortColumns){
            		console.log(sortColumns);
            	$scope.getPage();
            	});
            	$scope.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.page = newPage;
                    paginationOptions.count= pageSize;
                    url = api + "count="+ paginationOptions.count +"&page=" + paginationOptions.page 
    				+ "&whCd=" + $scope.whCd
    				+ "&itemCd=" + $scope.itemCd
    				+ "&itemNm=" + $scope.itemNm;
                    $scope.getPage();
                });
            	
        	}
          };
     $scope.getPage = function() {
	      	$http.get(url)
            .success(function (result) {
            		var count = Object.keys(result.data).length;
            	if(count > 0){
	            	$scope.gridOptions.totalItems = result.total;
            	}else{
            		$scope.nodata();
            	}
	            $scope.gridOptions.data = result.data;
            	
            });  
		} 
        $scope.search = function(whCd, itemCd, itemNm){
        		if(typeof itemCd == 'undefined'){itemCd = null}
        		if(typeof whCd == 'undefined'){whCd = null}
        		if(typeof itemNm == 'undefined'){itemNm = null}
        		
        		if(itemCd != null)
        		{
	        		if(parseInt(itemCd, 10) > 0 ? true : false){
	        			url = api + "count="+ paginationOptions.count +"&page=" + paginationOptions.page 
	        				+ "&whCd="+whCd+"&itemCd="+itemCd+"&itemNm="+itemNm;
	        			$scope.getPage();
	        		}else{
	        			
	        			alert("품목번호는 숫자형입니다.");
	        		}
        		}
        		else
        		{
        			url = api + "count="+ paginationOptions.count +"&page=" + paginationOptions.page 
    				+ "&whCd="+whCd+"&itemCd="+itemCd+"&itemNm="+itemNm;
        			$scope.getPage();
        		}
        	
        	console.log("whCd="+whCd+"itemCd="+itemCd+"itemNm="+itemNm);
        }
        
  
        $scope.nodata = function() {
            SweetAlert.swal({   
              title: '결과가 없습니다',   
              text: "검색한 결과가 없습니다.",   
              type: 'warning',   
              showCancelButton: false,   
              confirmButtonColor: '#DD6B55',   
              confirmButtonText: '닫기',
              closeOnConfirm: true
            },  function(){});
        };
        

        $scope.getPage();
        
    }
})();
