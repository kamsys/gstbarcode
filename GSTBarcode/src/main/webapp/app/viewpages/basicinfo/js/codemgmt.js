/**=========================================================
 * Module: basicinfo > 품목관리
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.basicinfo')
        .controller('CodeController', CodeController);

    CodeController.$inject = ['uiGridConstants', '$http', '$scope', '$log','SweetAlert', 'ngDialog'];
    function CodeController(uiGridConstants, $http, $scope, $log,SweetAlert, ngDialog) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

                   
          // Complex example
          // ----------------------------------- 

          var data = [];
          
           vm.gridOptions1 = {
              enableSorting: true,
              columnDefs: [
                           
                { name: 'No.', field:'name',width:60},
                { name: '기능', field:'name' ,width:70, enableSorting: false,
  	              cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
  	                    
  	              },
  	              cellTemplate: '<div class="col-md-12 col-centered"><button id="delBtn" type="button" class="btn-xs" ng-click="grid.appScope.deleteRow(grid,row)">수정</button></div>'
                },
                { name: '코드명', field:'name' ,width:150},
                { name: '출력명', field:'name' ,width:150},
                { name: '출력유무', field:'name' ,width:150},
                { name: '우선순위', field:'name' ,width:150},
                { name: '코드설명', field:'name' ,width:100}
              ]
            };
           
           	vm.gridOptions1.onRegisterApi = function(gridApi){
               vm.gridApi = gridApi;
            };
           
            $http.get('server/uigrid-100.json')
            .success(function (data) {
              vm.gridOptions1.data = data;
            });
            
        }
        
        $scope.demo4 = function(item) {
            SweetAlert.swal({   
              title: item + '을 삭제하시겠습니까?',   
              text: '재고가 있는경우 삭제할 수 없습니다.',   
              type: 'warning',   
              showCancelButton: true,   
              confirmButtonColor: '#DD6B55',   
              confirmButtonText: '삭제',
              closeOnConfirm: false
            },  function(){  
            	console.log("삭제로직 처리");
            	SweetAlert.swal('삭제되었습니다');
            });
        };
        
        $scope.deleteRow = function(grid, row){
        	console.log(grid);
        	console.log(row.entity.name);
        	$scope.demo4(row.entity.name);
        }
        
        $scope.addCode = function () {
	        ngDialog.openConfirm({
	          template: 'app/viewpages/basicinfo/partial/coderegister.html',
	          className: 'ngdialog-theme-default'
	        }).then(function (value) {
	          console.log('Modal promise resolved. Value: ', value);
	        }, function (reason) {
	          console.log('Modal promise rejected. Reason: ', reason);
	        });
	    };
        
        
    }
})();