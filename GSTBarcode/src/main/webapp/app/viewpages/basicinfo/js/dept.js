/**=========================================================
 * Module: basicinfo > 부서관리
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.basicinfo')
        .controller('DeptController', DeptController);

    DeptController.$inject = ['uiGridConstants', '$http', '$scope'];
    function DeptController(uiGridConstants, $http, $scope) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

                   
          // Complex example
          // ----------------------------------- 

          var data = [];
                    

          vm.gridOptions1 = {
              paginationPageSizes: [25, 50, 75],
              paginationPageSize: 25,
              enableHorizontalScrollbar: 1,
              enableFiltering: true,
              columnDefs: [
                { name: 'No.', field:'seq', width: '8%', enableFiltering: false},
                { name: '부서코드', field:'deptcode' },
                { name: '부서명', field:'deptname' },
                { name: '원가부서코드', field:'cstdeptcode' },
                { name: '원가부서코드명', field:'cstdeptname' },
                { name: '부서장사번', field:'heptheadno' },
                { name: '부서장명', field:'depthead' }
              ]
            };
           
            $http.get('getdepts')
            .success(function (result) {
              vm.gridOptions1.data = result.data;
            });

        }
    }
})();