/**=========================================================
 * Module: basicinfo > 사용자관리
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.basicinfo')
        .controller('UserController', UserController);

    UserController.$inject = ['uiGridConstants', '$http', '$scope', '$log', 'SweetAlert'];
    function UserController(uiGridConstants, $http, $scope, $log, SweetAlert) {
    	var vm = this;        
        var paginationOptions = {
        	    page: 1,
        	    count: 25,
        	    sort: null
        };
        
        var api = "getusers";
        var url = api;
        
        var hdrTmplt = '<div class="text-center" style="margin-top: 4px;">{{col.displayName}}</div>';
					      
					      
        
        console.log($scope);
        $scope.gridOptions = {
        	data: [],
        	paginationPageSizes: [25, 50, 75],
            paginationPageSize: 25,            
            enableFiltering: true,
            columnDefs: [
                         
			  { name: '사원번호', field:'empNo' ,width:'10%'},
              { name: '성명', field:'userName' ,width:"8%"},
              { name: '아이디', field:'userId', width:"10%"},
              { name: '부서', field:'deptName', width:"12%"},
              { name: '직급', field:'position' ,width:"10%", headerCellTemplate: hdrTmplt},
              { name: '연락처', field:'cntctNo' ,width:"7%", headerCellTemplate: hdrTmplt},
              
            ], 
            onRegisterApi: function(gridApi) {
            	$scope.gridApi = gridApi;
            	
        	}
        };
    	
        $scope.getPage = function(){        	
        	$http.get(url)
            .success(function (result) {
            	if(result.total > 0){
	            	$scope.gridOptions.totalItems = result.total;
            	}else{
            		$scope.nodata();
            	}
	            $scope.gridOptions.data = result.data;
            	
            });
    	}
                
        $scope.getPage();
        
    }
})();