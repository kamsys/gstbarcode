/**=========================================================
 * Module: basicinfo > 바코드관리
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.basicinfo')
        .controller('BarcodeController', BarcodeController);

    BarcodeController.$inject = ['$filter', '$http', 'editableOptions', 'editableThemes','$q', '$scope'];
    function BarcodeController($filter, $http, editableOptions, editableThemes, $q, $scope) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

                   
          // Complex example
          // ----------------------------------- 

        	var data = [];
        	
        	// editable row
            // ----------------------------------- 
            vm.users = [
              {id: 1, issueqty:0, name: 'awesome user1', status: 2, group: 4, groupName: 'admin'},
              {id: 2, issueqty:0, name: 'awesome user2', status: undefined, group: 3, groupName: 'vip'},
              {id: 3, issueqty:0, name: 'awesome user3', status: 2, group: null}
            ];



            vm.showStatus = function(user) {
              var selected = [];
              if(user.status) {
                selected = $filter('filter')(vm.statuses, {value: user.status});
              }
              return selected.length ? selected[0].text : 'Not set';
            };

            vm.checkName = function(data, id) {
              if (id === 2 && data !== 'awesome') {
                return 'Username 2 should be `awesome`';
              }
            };

            vm.saveUser = function(data, id) {
              //vm.user not updated yet
              angular.extend(data, {id: id});
              console.log('Saving user: ' + id);
              // return $http.post('/saveUser', data);
            };

            // remove user
            vm.removeUser = function(index) {
              vm.users.splice(index, 1);
            };

            // add user
            vm.addUser = function() {
              vm.inserted = {
                id: vm.users.length+1,
                name: '',
                status: null,
                group: null,
                isNew: true
              };
              vm.users.push(vm.inserted);
            };

            // editable column
            // ----------------------------------- 


            vm.saveColumn = function(column) {
              var results = [];
              angular.forEach(vm.users, function(/*user*/) {
                // results.push($http.post('/saveColumn', {column: column, value: user[column], id: user.id}));
                console.log('Saving column: ' + column);
              });
              return $q.all(results);
            };

            // editable table
            // ----------------------------------- 

            // filter users to show
            vm.filterUser = function(user) {
              return user.isDeleted !== true;
            };

            // mark user as deleted
            vm.deleteUser = function(id) {
              var filtered = $filter('filter')(vm.users, {id: id});
              if (filtered.length) {
                filtered[0].isDeleted = true;
              }
            };

            // cancel all changes
            vm.cancel = function() {
              for (var i = vm.users.length; i--;) {
                var user = vm.users[i];
                // undelete
                if (user.isDeleted) {
                  delete user.isDeleted;
                }
                // remove new 
                if (user.isNew) {
                  vm.users.splice(i, 1);
                }
              }
            };

            // save edits
            vm.saveTable = function() {
              var results = [];
              for (var i = vm.users.length; i--;) {
                var user = vm.users[i];
                // actually delete user
                if (user.isDeleted) {
                  vm.users.splice(i, 1);
                }
                // mark as not new 
                if (user.isNew) {
                  user.isNew = false;
                }

                // send on server
                // results.push($http.post('/saveUser', user));
                console.log('Saving Table...');
              }

              return $q.all(results);
            };

        }
    }
})();