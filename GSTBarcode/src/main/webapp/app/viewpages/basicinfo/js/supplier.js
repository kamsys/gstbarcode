/**=========================================================
 * Module: basicinfo > 거래처관리
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.basicinfo')
        .controller('SupplierController', SupplierController);

    SupplierController.$inject = ['uiGridConstants', '$http', '$scope', '$log','modal'];
    function SupplierController(uiGridConstants, $http, $scope, $log, modal) {
        var vm = this;
        var myModal = new modal();
        
        activate();

        ////////////////

        function activate() {

                   
          // Complex example
          // ----------------------------------- 

          var data = [];
          
           vm.gridOptions1 = {
              paginationPageSizes: [25, 50, 75],
              paginationPageSize: 25,
              enableHorizontalScrollbar: 1,
              enableFiltering: true,
              columnDefs: [
                { name: 'No.', field:'seq', width: '5%', enableFiltering: false},
                { name: '거래처코드', width: '10%', field:'suppno', enableColumnResizing: true },
                { name: '사업자번호', field:'taxno', width: '10%' },
                { name: '거래처명', field:'comname' },
                { name: '대표자', field:'headname', width: '10%' },
                { name: '주소', field:'suppaddr',  enableColumnResizing: true }
              ]
            };
           
            $http.get('getsuppliers')
            .success(function (result) {
              vm.gridOptions1.data = result.data;
            });

        }
        
        $scope.showModal = function() {
	        myModal.open('거래처 관리');
	    };
    }
})();