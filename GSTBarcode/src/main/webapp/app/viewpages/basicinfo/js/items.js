/**=========================================================
 * Module: basicinfo > 품목관리
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.basicinfo')
        .controller('ItemsController', ItemsController);

    ItemsController.$inject = ['uiGridConstants', '$http', '$scope', '$interval', '$log','SweetAlert'];
    function ItemsController(uiGridConstants, $http, $scope, $interval, $log,SweetAlert) {
        var vm = this;        
        var paginationOptions = {
        	    page: 1,
        	    count: 25,
        	    sort: null
        };
        
        var api = "getitems?";
        var url = api + "count="+ paginationOptions.count +"&page=" + paginationOptions.page;
        
        var hdrTmplt = '<div class="text-center" style="margin-top: 4px;">{{col.displayName}}</div>';
					      
					      
        
        console.log($scope);
        $scope.gridOptions = {
        	data: [],
        	paginationPageSizes: [25, 50, 75],
            paginationPageSize: 25,
            useExternalPagination: true,
            useExternalSorting: true,
            columnDefs: [
                         
			  { name: '품목번호', field:'itemNo' ,width:'10%'},
              { name: '약식품번', field:'itemSno' ,width:"5%"},
              { name: '내역1', field:'itemName', enableSorting: false, headerCellTemplate: hdrTmplt},
              { name: '내역2', field:'itemSpec', enableSorting: false, headerCellTemplate: hdrTmplt},
              { name: '재고유형', field:'itemType' ,width:"10%", headerCellTemplate: hdrTmplt},
              { name: 'GL범주', field:'glType' ,width:"7%", headerCellTemplate: hdrTmplt},
              { name: '주단위', field:'unit' ,width:"7%", headerCellTemplate: hdrTmplt},
              { name: '구매단위', field:'buyUnit' ,width:"7%", headerCellTemplate: hdrTmplt},
              { name: '변환계수', field:'convFactor' ,width:"7%", headerCellTemplate: hdrTmplt}
              
            ], 
            onRegisterApi: function(gridApi) {
            	$scope.gridApi = gridApi;
            	$scope.gridApi.core.on.sortChanged( $scope, function(grid, sortColumns){
            		console.log(sortColumns);
            		$scope.getPage();
            	});
            	$scope.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.page = newPage;
                    paginationOptions.count= pageSize;
                    url = api + "count="+ paginationOptions.count +"&page=" + paginationOptions.page 
    				+ "&opt=" + $scope.opt
    				+ "&keyword=" + $scope.keyword;
                    $scope.getPage();
                });
            	
        	}
        };
    	
        $scope.getPage = function(){        	
        	$http.get(url)
            .success(function (result) {
            	if(result.total > 0){
	            	$scope.gridOptions.totalItems = result.total;
            	}else{
            		$scope.nodata();
            	}
	            $scope.gridOptions.data = result.data;
            	
            });
    	}
        
        $scope.searchItems = function(opt, keyword){
        	
        	if(opt === 'no'){
        		if(parseInt(keyword, 10) > 0 ? true : false){
        			url = api + "count="+ paginationOptions.count +"&page=" + paginationOptions.page 
        				+ "&opt=no&keyword=" + keyword;
        			$scope.getPage();
        		}else{        			
        			alert("품목번호는 숫자형입니다.");
        		}
        	}else if(opt === 'name'){
        		if(keyword.length > 1){
        			url = api + "count="+ paginationOptions.count +"&page=" + paginationOptions.page 
    				+ "&opt=name&keyword=" + keyword;
        			$scope.getPage();
        		}else{
        			alert("검색어는 2자이상 입력하세요");
        		}
        	}else{
        		paginationOptions.count = 25;
        		paginationOptions.page = 1;
        		$scope.opt = undefined;
        		$scope.keyword = undefined;
        		url = api + "count="+ paginationOptions.count +"&page=" + paginationOptions.page;
        		$scope.getPage();
        	}
        	console.log(opt + "=" + keyword);
        }
        
        $scope.getPage();
        
        $scope.nodata = function() {
            SweetAlert.swal({   
              title: '결과가 없습니다',   
              text: $scope.keyword + " 로 검색한 결과가 없습니다.",   
              type: 'warning',   
              showCancelButton: false,   
              confirmButtonColor: '#DD6B55',   
              confirmButtonText: '닫기',
              closeOnConfirm: true
            },  function(){});
        };
                
    }
})();