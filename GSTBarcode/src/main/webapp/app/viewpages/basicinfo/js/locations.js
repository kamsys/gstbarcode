/**=========================================================
 * Module: basicinfo > 품목관리
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.basicinfo')
        .controller('LocationController', LocationController);

    LocationController.$inject = ['uiGridConstants', '$http', '$scope', '$log','SweetAlert'];
    function LocationController(uiGridConstants, $http, $scope, $log,SweetAlert) {
        var srchOptions = {
        	    whcode: 'G10'
        };
        
        var api = "getlocations?";
        var url = api + "whcode=" + srchOptions.whcode;
        
        var hdrTmplt = '<div class="text-center" style="margin-top: 4px;">{{col.displayName}}</div>';
					      
        $scope.gridOptions = {
        	data: [],
            columnDefs: [
                         
			  { name: '번호', field:'$index' ,width:'5%', headerCellTemplate: hdrTmplt},
              { name: '창고', field:'whcode' ,width:"30%", headerCellTemplate: hdrTmplt},
              { name: '장소', field:'loc', enableSorting: true}              
              
            ], 
            onRegisterApi: function(gridApi) {
            	$scope.gridApi = gridApi;
        	}
        };
    	
        $scope.getPage = function(){        	
        	$http.get(url)
            .success(function (result) {            	
	            $scope.gridOptions.data = result.data;
            	
            });
    	}
        
        $scope.searchItems = function(opt, keyword){
        	
        	if(opt.length > 1){
        		srchOptions.whcode = opt;
    			url = api + "whcode=" + srchOptions.whcode; 
    			$scope.getPage();
        	}else{
        		srchOptions.whcode = "G10";
    			url = api + "whcode=" + srchOptions.whcode; 
        	}
        	$scope.getPage();
        	console.log(opt + "=" + keyword);
        }
        
        
        $scope.nodata = function() {
            SweetAlert.swal({   
              title: '결과가 없습니다',   
              text: $scope.keyword + " 로 검색한 결과가 없습니다.",   
              type: 'warning',   
              showCancelButton: false,   
              confirmButtonColor: '#DD6B55',   
              confirmButtonText: '닫기',
              closeOnConfirm: true
            },  function(){});
        };
        
        //initialize
        $scope.getPage();
        
    }
})();