/**=========================================================
 * Module: receiving > 입고조회
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.receiving')
        .controller('DefectController', DefectController);

    DefectController.$inject = ['$resource', '$http', '$scope', '$log','ngDialog' ,'ngTableParams',
                              'ngTableDataService'];
    function DefectController($resource, $http, $scope, $log, ngDialog, ngTableParams,
    						ngTableDataService) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

          // AJAX
          
          var Api = $resource('server/table-data.json');

          vm.tableParams5 = new ngTableParams({
              page: 1,            // show first page
              count: 10           // count per page
          }, {
              total: 0,           // length of data
              counts: [],         // hide page counts control
              getData: function($defer, params) {
                  
                  // Service using cache to avoid mutiple requests
                  ngTableDataService.getData( $defer, params, Api);
                  
                  /* direct ajax request to api (perform result pagination on the server)
                  Api.get(params.url(), function(data) {
                      $timeout(function() {
                          // update table params
                          params.total(data.total);
                          // set new data
                          $defer.resolve(data.result);
                      }, 500);
                  });
                  */
              }
          });
            
        }
        
        $scope.viewPO = function () {
	        ngDialog.open({
	          template: 'app/viewpages/receiving/partial/viewPurchaseOrder.html',
	          className: 'ngdialog-theme-custom',
	          controller: 'PoViewController',
	          scope: $scope,
	          cache: false
	        })
	    };
        
        
    }
})();


/**=========================================================
 * Module: receiving > 발주서 상세조회
  =========================================================*/

(function() {
	'use strict';
	
	angular
	.module('app.receiving')
	.controller('PoViewController', PoViewController);
	
	PoViewController.$inject = ['uiGridConstants', '$http', '$scope', '$log', 'ngDialog'];
	function PoViewController(uiGridConstants, $http, $scope, $log, ngDialog) {
		var vm = this;
		
		activate();
		
		////////////////
		
		function activate() {
			
			
			// Complex example
			// ----------------------------------- 
			
			var data = [];
			
			vm.gridOptions = {
					showColumnFooter: true,
					columnDefs: [
					             
					             { name: 'No.', field:'name',width:60, enableSorting: false},
					             { name: '품번', field:'name' ,width:100},
					             { name: '품명', field:'name' ,width:150, 
					            	 cellTooltip: function( row, col ) {
					                     return row.entity.name;
					                 }
					             },
					             { name: '규격', field:'name' ,width:150},
					             { name: '단위', field:'name' ,width:70},
					             { name: '수량', field:'name' ,width:70},
					             { name: '단가', field:'name' ,width:80, 
					            	 footerCellTemplate: '<div class="ui-grid-cell-contents" style="background-color: Gray;color: White">Total:</div>'
					             },
					             { name: '금액', field:'name' ,width:80,
					            	 aggregationType: uiGridConstants.aggregationTypes.sum,
					            	 aggregationHideLabel: true
					             },
					             
					             { name: '납기일', field:'name' ,width:80}
					            ]
			};
			
			vm.gridOptions.onRegisterApi = function(gridApi){
				vm.gridApi = gridApi;
			};
			
			$http.get('server/uigrid-100.json')
			.success(function (data) {
				vm.gridOptions.data = data;
			});
			
		}
	
	}
})();