/**=========================================================
 * Module: receiving > 입고조회
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.receiving')
        .controller('RecvController', RecvController);

    RecvController.$inject = ['$resource', '$http', '$scope', '$log','ngDialog' ,'ngTableParams',
                              'ngTableDataService'];
    function RecvController($resource, $http, $scope, $log, ngDialog, ngTableParams,
    						ngTableDataService) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

                   
          // Complex example
          // ----------------------------------- 

          var data = [
              {name: 'Moroni',  age: 50, money: -10   },
              {name: 'Tiancum', age: 43, money: 120   },
              {name: 'Jacob',   age: 27, money: 5.5   },
              {name: 'Nephi',   age: 29, money: -54   },
              {name: 'Enos',    age: 34, money: 110   },
              {name: 'Tiancum', age: 43, money: 1000  },
              {name: 'Jacob',   age: 27, money: -201  },
              {name: 'Nephi',   age: 29, money: 100   },
              {name: 'Enos',    age: 34, money: -52.5 },
              {name: 'Tiancum', age: 43, money: 52.1  },
              {name: 'Jacob',   age: 27, money: 110   },
              {name: 'Nephi',   age: 29, money: -55   },
              {name: 'Enos',    age: 34, money: 551   },
              {name: 'Tiancum', age: 43, money: -1410 },
              {name: 'Jacob',   age: 27, money: 410   },
              {name: 'Nephi',   age: 29, money: 100   },
              {name: 'Enos',    age: 34, money: -100  }
          ];
          
          // AJAX
          
          var Api = $resource('server/table-data.json');

          vm.tableParams5 = new ngTableParams({
              page: 1,            // show first page
              count: 10           // count per page
          }, {
              total: 0,           // length of data
              counts: [],         // hide page counts control
              getData: function($defer, params) {
                  
                  // Service using cache to avoid mutiple requests
                  ngTableDataService.getData( $defer, params, Api);
                  
                  /* direct ajax request to api (perform result pagination on the server)
                  Api.get(params.url(), function(data) {
                      $timeout(function() {
                          // update table params
                          params.total(data.total);
                          // set new data
                          $defer.resolve(data.result);
                      }, 500);
                  });
                  */
              }
          });
            
        }
        
        $scope.viewPO = function () {
	        ngDialog.open({
	          template: 'app/viewpages/receiving/partial/viewPurchaseOrder.html',
	          className: 'ngdialog-theme-custom',
	          controller: 'PoViewController',
	          scope: $scope,
	          cache: false
	        })
	    };
        
        
    }
})();


/**=========================================================
 * Module: receiving > 발주서 상세조회
  =========================================================*/

(function() {
	'use strict';
	
	angular
	.module('app.receiving')
	.controller('PoViewController', PoViewController);
	
	PoViewController.$inject = ['uiGridConstants', '$http', '$scope', '$log', 'ngDialog'];
	function PoViewController(uiGridConstants, $http, $scope, $log, ngDialog) {
		var vm = this;
		
		activate();
		
		////////////////
		
		function activate() {
			
			
			// Complex example
			// ----------------------------------- 
			
			var data = [];
			
			vm.gridOptions = {
					showColumnFooter: true,
					columnDefs: [
					             
					             { name: 'No.', field:'name',width:60, enableSorting: false},
					             { name: '품번', field:'name' ,width:100},
					             { name: '품명', field:'name' ,width:150, 
					            	 cellTooltip: function( row, col ) {
					                     return row.entity.name;
					                 }
					             },
					             { name: '규격', field:'name' ,width:150},
					             { name: '단위', field:'name' ,width:70},
					             { name: '수량', field:'name' ,width:70},
					             { name: '단가', field:'name' ,width:80, 
					            	 footerCellTemplate: '<div class="ui-grid-cell-contents" style="background-color: Gray;color: White">Total:</div>'
					             },
					             { name: '금액', field:'name' ,width:80,
					            	 aggregationType: uiGridConstants.aggregationTypes.sum,
					            	 aggregationHideLabel: true
					             },
					             
					             { name: '납기일', field:'name' ,width:80}
					            ]
			};
			
			vm.gridOptions.onRegisterApi = function(gridApi){
				vm.gridApi = gridApi;
			};
			
			$http.get('server/uigrid-100.json')
			.success(function (data) {
				vm.gridOptions.data = data;
			});
			
		}
	
	}
})();