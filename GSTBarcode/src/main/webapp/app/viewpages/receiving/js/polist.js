/**=========================================================
 * Module: receiving > 발주서조회
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.receiving')
        .controller('PoController', PoController);

    PoController.$inject = ['uiGridConstants', '$http', '$scope', '$log','SweetAlert', 'ngDialog'];
    function PoController(uiGridConstants, $http, $scope, $log,SweetAlert, ngDialog) {
    	
    	var vm = this;        
        var paginationOptions = {
        	    page: 1,
        	    count: 25,
        	    sort: null
        };
        
        var api = "getpolist";
        var url = api;
        
        var hdrTmplt = '<div class="text-center" style="margin-top: 4px;">{{col.displayName}}</div>';
					      
					      
        
        console.log($scope);
        $scope.gridOptions = {
        	data: [],
        	paginationPageSizes: [25, 50, 75],
            paginationPageSize: 25,            
            enableFiltering: true,
            columnDefs: [
                         
			              
                { name: 'No.', field:'seq',width:60, enableSorting: false, enablePinning: false, headerCellTemplate: hdrTmplt},
                { name: '기능', width:160, 
                  enableSorting: false, enablePinning: false,
  	              cellTemplate: '<div class="col-md-12 text-center col-centered row p0">'
  	            	  + '<button id="redvBtn" type="button" class="btn-xs text-sm btn-success" ng-click="grid.appScope.recvItems(grid,row)">입고처리</button>'
  	            	  + '<button id="bcdBtn" type="button" class="btn-xs text-sm btn-default" ng-click="grid.appScope.goBarcode(grid,row)">바코드발행</button></div>'
                },
                { name: '발주코드', field:'opCode' ,width:80,
                  enablePinning: true,	
                  cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.viewPO(row.entity.name)">{{COL_FIELD}}</a></div>'	
                },
                { name: '요청코드', field:'orCode' ,width:80, enablePinning: false	
                },
                { name: '발주요청', field:'chrgReq' ,width:80, enablePinning: false},
                { name: '공급자', field:'suppName' ,width:80, enablePinning: false, headerCellTemplate: hdrTmplt},
                { name: '발주날짜', field:'poDate' ,width:80, enablePinning: false},
                { name: '발주담당', field:'chrgOrder' ,width:80, enablePinning: false, headerCellTemplate: hdrTmplt},
                { name: '창고', field:'whCode' ,width:80, enablePinning: false, headerCellTemplate: hdrTmplt}
              
            ], 
            onRegisterApi: function(gridApi) {
            	$scope.gridApi = gridApi;
            	
        	}
        };
    	
        $scope.getPage = function(){        	
        	$http.get(url)
            .success(function (result) {
            	if(result.data.length > 0){
	            	$scope.gridOptions.totalItems = result.data.length;
            	}else{
            		$scope.nodata();
            	}
	            $scope.gridOptions.data = result.data;
            	
            });
    	}
                
        $scope.nodata = function() {
            SweetAlert.swal({   
              title: '결과가 없습니다.',   
              text: '조회 결과가 없습니다.',   
              type: 'warning',   
              showCancelButton: false,   
              confirmButtonColor: '#DD6B55',   
              confirmButtonText: '닫기',
              closeOnConfirm: true
            },  function(){});
        };
        
        $scope.recvItems = function(grid, row){
        	console.log(grid);
        	console.log(row.entity.name);
        	$scope.demo4(row.entity.name);
        }

        $scope.goBarcode = function(grid, row){
        	console.log(grid);
        	console.log(row.entity.name);
        	$scope.demo4(row.entity.name);
        }
        
        $scope.viewPO = function () {
	        ngDialog.open({
	          template: 'app/viewpages/receiving/partial/viewPurchaseOrder.html',
	          className: 'ngdialog-theme-custom',
	          controller: 'PoViewController',
	          scope: $scope,
	          cache: false
	        })
	    };
	    
	    $scope.getPage();        
        
    }
})();


/**=========================================================
 * Module: receiving > 발주서 상세조회
  =========================================================*/

(function() {
	'use strict';
	
	angular
	.module('app.receiving')
	.controller('PoViewController', PoViewController);
	
	PoViewController.$inject = ['uiGridConstants', '$http', '$scope', '$log', 'ngDialog'];
	function PoViewController(uiGridConstants, $http, $scope, $log, ngDialog) {
		var vm = this;
		
		activate();
		
		////////////////
		
		function activate() {
			
			
			// Complex example
			// ----------------------------------- 
			
			var data = [];
			
			vm.gridOptions = {
					showColumnFooter: true,
					columnDefs: [
					             
					             { name: 'No.', field:'name',width:60, enableSorting: false},
					             { name: '품번', field:'name' ,width:100},
					             { name: '품명', field:'name' ,width:150, 
					            	 cellTooltip: function( row, col ) {
					                     return row.entity.name;
					                 }
					             },
					             { name: '규격', field:'name' ,width:150},
					             { name: '단위', field:'name' ,width:70},
					             { name: '수량', field:'name' ,width:70},
					             { name: '단가', field:'name' ,width:80, 
					            	 footerCellTemplate: '<div class="ui-grid-cell-contents" style="background-color: Gray;color: White">Total:</div>'
					             },
					             { name: '금액', field:'name' ,width:80,
					            	 aggregationType: uiGridConstants.aggregationTypes.sum,
					            	 aggregationHideLabel: true
					             },
					             
					             { name: '납기일', field:'name' ,width:80}
					            ]
			};
			
			vm.gridOptions.onRegisterApi = function(gridApi){
				vm.gridApi = gridApi;
			};
			
			$http.get('server/uigrid-100.json')
			.success(function (data) {
				vm.gridOptions.data = data;
			});
			
		}
	
	}
})();