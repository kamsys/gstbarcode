/**=========================================================
 * Module: receiving > 발주입고처리
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.receiving')
        .controller('NoPoReceivingController', PoReceivingController);

    PoReceivingController.$inject =['$filter', '$http', 'editableOptions', 'editableThemes','$q', 'parseNumber',
                                    '$scope'];
    function PoReceivingController($filter, $http, editableOptions, editableThemes, $q, parseNumber,
    								$scope) {
        var vm = this;
        vm.title = 'Controller';

        activate();

        ////////////////

        function activate() {
        	
        	// editable row
            // ----------------------------------- 
            vm.users = [
              {id: 1, name: 'awesome user1', itemno: 115312, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 2, name: 'awesome user1', itemno: 113232, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 3, name: 'awesome user1', itemno: 115243, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 4, name: 'awesome user1', itemno: 115454, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 5, name: 'awesome user1', itemno: 113257, unitprice: 4000, orderqty: 3, recvqty:0}
            ];

           
            vm.saveUser = function(data, id) {
              //vm.user not updated yet
              angular.extend(data, {id: id});
              console.log('Saving user: ' + id);
              // return $http.post('/saveUser', data);
            };

            // remove user
            vm.removeUser = function(index) {
              vm.users.splice(index, 1);
            };

            // add user
            vm.addUser = function() {
              vm.inserted = {
                id: vm.users.length+1,
                name: '',
                itemno: null,
                unitprice: null,
                orderqty: 0,
                recvqty: 0
              };
              vm.users.push(vm.inserted);
            };            
        }
        
    }
})();
