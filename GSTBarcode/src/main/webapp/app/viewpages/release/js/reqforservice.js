/**=========================================================
 * Module: release > 서비스출고요청 조회
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.release')
        .controller('RequestServiceController', RequestServiceController);

    RequestServiceController.$inject = ['uiGridConstants', '$http', '$scope', '$log','SweetAlert', 'ngDialog'];
    function RequestServiceController(uiGridConstants, $http, $scope, $log,SweetAlert, ngDialog) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

                   
          // Complex example
          // ----------------------------------- 

          var data = [];
          
           vm.gridOptions1 = {
              enableSorting: true,
              columnDefs: [
                           
                { name: 'No.', field:'name',width:60, enableSorting: false, enablePinning: false},
                { name: '기능', field:'name' ,width:80, 
                    enableSorting: false, enablePinning: false,
    	              cellTemplate: '<div class="col-md-12 text-center col-centered row p0">'
    	            	  + '<button id="redvBtn" type="button" class="btn-xs text-sm btn-success" ng-click="grid.appScope.recvItems(grid,row)">출고처리</button>'
                },
                { name: '수주유형', field:'name' ,width:150, enablePinning: false},
                { name: '수주번호', field:'name' ,width:150,
                  enablePinning: true,	
                  cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.viewPO(row.entity.name)">{{COL_FIELD}}</a></div>'	
                },
                { name: '출고요청일', field:'name' ,width:150, enablePinning: false},
                { name: '출고유형', field:'name' ,width:150, enablePinning: false},
                { name: '요청자', field:'name' ,width:100, enablePinning: false}
              ]
            };
           
           	vm.gridOptions1.onRegisterApi = function(gridApi){
               vm.gridApi = gridApi;
            };
           
            $http.get('server/uigrid-100.json')
            .success(function (data) {
              vm.gridOptions1.data = data;
            });
            
        }
                       
        $scope.recvItems = function(grid, row){
        	console.log(grid);
        	console.log(row.entity.name);
        	$scope.viewPO();
        }

        $scope.goBarcode = function(grid, row){
        	console.log(grid);
        	console.log(row.entity.name);
        	$scope.demo4(row.entity.name);
        }
        
        $scope.viewPO = function () {
	        ngDialog.open({
	          template: 'app/viewpages/release/partial/viewforService.html',
	          className: 'ngdialog-theme-custom',
	          controller: 'FroductViewController',
	          scope: $scope,
	          cache: false
	        })
	    };
        
        
    }
})();


/**=========================================================
 * Module: release > 서비스요청 상세조회
  =========================================================*/

(function() {
	'use strict';
	
	angular
	.module('app.release')
	.controller('ForServiceViewController', ForServiceViewController);
	
	ForServiceViewController.$inject = ['uiGridConstants', '$http', '$scope', '$log', 'ngDialog'];
	function ForServiceViewController(uiGridConstants, $http, $scope, $log, ngDialog) {
		var vm = this;
		
		activate();
		
		////////////////
		
		function activate() {
			
			
			// Complex example
			// ----------------------------------- 
			
			var data = [];
			
			vm.gridOptions = {
					showColumnFooter: false,
					columnDefs: [
					             
					             { name: 'No.', field:'name',width:60, enableSorting: false},
					             { name: '품번', field:'name' ,width:100},
					             { name: '품명', field:'name' ,width:150, 
					            	 cellTooltip: function( row, col ) {
					            		 return row.entity.name;
					            	 }
					             },
					             { name: '규격', field:'name' ,width:150},
					             { name: '단위', field:'name' ,width:70},
					             { name: 'PJT', field:'name' ,width:70},
					             { name: '공정', field:'name' ,width:80},
					             { name: 'S/N', field:'name' ,width:80},					             
					             { name: '라인번호', field:'name' ,width:80}
					             ]
			};
			
			vm.gridOptions.onRegisterApi = function(gridApi){
				vm.gridApi = gridApi;
			};
			
			$http.get('server/uigrid-100.json')
			.success(function (data) {
				vm.gridOptions.data = data;
			});
			
		}
		
	}
})();