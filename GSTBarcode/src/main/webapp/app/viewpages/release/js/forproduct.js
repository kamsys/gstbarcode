/**=========================================================
 * Module: release > 생산출고 조회
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.release')
        .controller('FroductController', FroductController);

    FroductController.$inject = ['uiGridConstants', '$http', '$scope', '$log','SweetAlert', 'ngDialog'];
    function FroductController(uiGridConstants, $http, $scope, $log,SweetAlert, ngDialog) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

                   
          // Complex example
          // ----------------------------------- 

          var data = [];
          
           vm.gridOptions1 = {
              enableSorting: true,
              paginationPageSizes: [25, 50],
              paginationPageSize: 25,
              columnDefs: [
                           
                { name: 'No.', field:'name',width:60, enableSorting: false, enablePinning: false},
                
                { name: 'WO', field:'name' ,width:150,
                  enablePinning: true,	
                  cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.viewPO(row.entity.name)">{{COL_FIELD}}</a></div>'	
                },
                { name: '출고요청일', field:'name' ,width:150, enablePinning: false},
                { name: '출고상태', field:'name' ,width:150, enablePinning: false},
                { name: '모품번호', field:'name' ,width:150, enablePinning: false},
                { name: '요청자', field:'name' ,width:100, enablePinning: false},
                { name: '사업장', field:'name' ,width:100, enablePinning: false}
              ]
            };
           
           	vm.gridOptions1.onRegisterApi = function(gridApi){
               vm.gridApi = gridApi;
            };
           
            $http.get('server/uigrid-100.json')
            .success(function (data) {
              vm.gridOptions1.data = data;
            });
            
        }
        
        $scope.demo4 = function(item) {
            SweetAlert.swal({   
              title: item + '을 삭제하시겠습니까?',   
              text: '재고가 있는경우 삭제할 수 없습니다.',   
              type: 'warning',   
              showCancelButton: true,   
              confirmButtonColor: '#DD6B55',   
              confirmButtonText: '삭제',
              closeOnConfirm: false
            },  function(){  
            	console.log("삭제로직 처리");
            	SweetAlert.swal('삭제되었습니다');
            });
        };
        
        $scope.recvItems = function(grid, row){
        	console.log(grid);
        	console.log(row.entity.name);
        	$scope.demo4(row.entity.name);
        }

        $scope.goBarcode = function(grid, row){
        	console.log(grid);
        	console.log(row.entity.name);
        	$scope.demo4(row.entity.name);
        }
        
        $scope.viewPO = function () {
	        ngDialog.open({
	          template: 'app/viewpages/release/partial/viewFroduct.html',
	          className: 'ngdialog-theme-custom',
	          controller: 'FroductViewController',
	          scope: $scope,
	          cache: false
	        })
	    };
        
        
    }
})();


/**=========================================================
 * Module: release > 생산요청 상세조회
  =========================================================*/

(function() {
	'use strict';
	
	angular
	.module('app.receiving')
	.controller('FroductViewController', FroductViewController);
	
	FroductViewController.$inject = ['uiGridConstants', '$http', '$scope', '$log', 'ngDialog'];
	function FroductViewController(uiGridConstants, $http, $scope, $log, ngDialog) {
		var vm = this;
		
		activate();
		
		////////////////
		
		function activate() {
			
			
			// Complex example
			// ----------------------------------- 
			
			var data = [];
			
			vm.gridOptions = {
					showColumnFooter: false,
					columnDefs: [
					             
					             { name: 'No.', field:'name',width:60, enableSorting: false},
					             { name: '품번', field:'name' ,width:100},
					             { name: '품명', field:'name' ,width:150, 
					            	 cellTooltip: function( row, col ) {
					                     return row.entity.name;
					                 }
					             },
					             { name: '규격', field:'name' ,width:150},
					             { name: '단위', field:'name' ,width:70},
					             { name: '필요량', field:'name' ,width:70},
					             { name: '기출고', field:'name' ,width:80},
					             { name: '출고', field:'name' ,width:80},					             
					             { name: '로케이션', field:'name' ,width:80}
					            ]
			};
			
			vm.gridOptions.onRegisterApi = function(gridApi){
				vm.gridApi = gridApi;
			};
			
			$http.get('server/uigrid-100.json')
			.success(function (data) {
				vm.gridOptions.data = data;
			});
			
		}
	
	}
})();