/**=========================================================
 * Module: release > Processing for product
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.release')
        .controller('WoFroductController', WoFroductController);

    WoFroductController.$inject =['$filter', '$http', 'editableOptions', 'editableThemes','$q',
                                    '$scope'];
    function WoFroductController($filter, $http, editableOptions, editableThemes, $q, 
    								$scope) {
        var vm = this;
        vm.title = 'Controller';

        activate();

        ////////////////

        function activate() {
        	
        	// editable row
            // ----------------------------------- 
            vm.users = [
              {id: 1, name: 'awesome user1', itemno: 115312, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 2, name: 'awesome user1', itemno: 113232, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 3, name: 'awesome user1', itemno: 115243, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 4, name: 'awesome user1', itemno: 115454, unitprice: 4000, orderqty: 3, recvqty:0},
              {id: 5, name: 'awesome user1', itemno: 113257, unitprice: 4000, orderqty: 3, recvqty:0}
            ];

           
            vm.saveUser = function(data, id) {
              //vm.user not updated yet
              angular.extend(data, {id: id});
              console.log('Saving user: ' + id);
              // return $http.post('/saveUser', data);
            };

            // remove user
            vm.removeUser = function(index) {
              vm.users.splice(index, 1);
            };

            // add user
            vm.addUser = function() {
              vm.inserted = {
                id: vm.users.length+1,
                name: '',
                status: null,
                group: null,
                isNew: true
              };
              vm.users.push(vm.inserted);
            };

            vm.withComma = function(number){
            	console.log("number with comma");
            	return parseNumber.withComma(number);
            };
        }
        
    }
})();
