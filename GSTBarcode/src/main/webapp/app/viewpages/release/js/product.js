/**=========================================================
 * Module: release > 제품출하 조회
  =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.release')
        .controller('ProductController', ProductController);

    ProductController.$inject = ['uiGridConstants', '$http', '$scope', '$log','SweetAlert', 'ngDialog'];
    function ProductController(uiGridConstants, $http, $scope, $log,SweetAlert, ngDialog) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

                   
          // Complex example
          // ----------------------------------- 

          var data = [];
          
           vm.gridOptions1 = {
              enableSorting: true,
              paginationPageSizes: [25, 50],
              paginationPageSize: 25,
              columnDefs: [
                           
                { name: 'No.', field:'name',width:60, enableSorting: false, enablePinning: false},
                
                { name: '수주번호', field:'name' ,width:150,
                	enablePinning: true,	
                	cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.viewService(row.entity.name)">{{COL_FIELD}}</a></div>'	
                },
                { name: '수주유형', field:'name' ,width:150, enablePinning: false},
                { name: 'S/N', field:'name' ,width:100, enablePinning: false},
                { name: '라인번호', field:'name' ,width:150, enablePinning: false},
                { name: '단위', field:'name' ,width:100, enablePinning: false},
                { name: '출고량', field:'name' ,width:100, enablePinning: false},
                { name: '출고일', field:'name' ,width:100, enablePinning: false},
                { name: '기준', field:'name' ,width:100, enablePinning: false},
                { name: '대상', field:'name' ,width:100, enablePinning: false}
              ]
            };
           
           	vm.gridOptions1.onRegisterApi = function(gridApi){
               vm.gridApi = gridApi;
            };
           
            $http.get('server/uigrid-100.json')
            .success(function (data) {
              vm.gridOptions1.data = data;
            });
            
        }
        
        $scope.viewService = function () {
	        ngDialog.open({
	          template: 'app/viewpages/release/partial/viewProduct.html',
	          className: 'ngdialog-theme-custom',
	          controller: 'ProductViewController',
	          scope: $scope,
	          cache: false
	        })
	    };
        
        
    }
})();


/**=========================================================
 * Module: release > 제품출하 상세조회
  =========================================================*/

(function() {
	'use strict';
	
	angular
	.module('app.receiving')
	.controller('ProductViewController', ProductViewController);
	
	ProductViewController.$inject = ['uiGridConstants', '$http', '$scope', '$log', 'ngDialog'];
	function ProductViewController(uiGridConstants, $http, $scope, $log, ngDialog) {
		var vm = this;
		
		activate();
		
		////////////////
		
		function activate() {
			
			
			// Complex example
			// ----------------------------------- 
			
			var data = [];
			
			vm.gridOptions = {
					showColumnFooter: false,
					columnDefs: [
					             
					             { name: 'No.', field:'name',width:60, enableSorting: false},
					             { name: '품번', field:'name' ,width:100},
					             { name: '품명', field:'name' ,width:150, 
					            	 cellTooltip: function( row, col ) {
					                     return row.entity.name;
					                 }
					             },
					             { name: '규격', field:'name' ,width:150},
					             { name: '단위', field:'name' ,width:70},
					             { name: '필요량', field:'name' ,width:70},
					             { name: '기출고', field:'name' ,width:80},
					             { name: '출고', field:'name' ,width:80},					             
					             { name: '로케이션', field:'name' ,width:80}
					            ]
			};
			
			vm.gridOptions.onRegisterApi = function(gridApi){
				vm.gridApi = gridApi;
			};
			
			$http.get('server/uigrid-100.json')
			.success(function (data) {
				vm.gridOptions.data = data;
			});
			
		}
	
	}
})();