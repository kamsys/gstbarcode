// A GSTWMS Common factory, Services

(function(){
	'use strict';

	angular
		.module('app.gstwms')
		.directive('enterEvent', enterEvent);
	
	enterEvent.$inject = ['$timeout', '$window'];
    function enterEvent ($timeout, $window) {
    	
    	 var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;
        
    	function link (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.myEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    }
	
})();

(function() {
    'use strict';

    angular
        .module('app.gstwms')
        .factory('modal', modal);

    modal.$inject = ['$http','$compile', '$rootScope'];
    function modal($http, $compile, $rootScope) {
    	 return function() {
		    var elm;
		    var modal = {
		      open: function(message) {
		 
		        var html = '<div class="modal" ng-style="modalStyle">{{modalStyle}}<div class="modal-dialog"><div class="modal-content"><div class="modal-header"></div><div class="modal-body">'
		        	+ message
		        	+ '</div><div class="modal-footer"><button id="buttonClose" class="btn btn-primary" ng-click="close()">Close</button></div></div></div></div>';
		        elm = angular.element(html);
		        angular.element(document.body).prepend(elm);
		 
		        $rootScope.close = function() {
		          modal.close();
		        };
		        
		        $rootScope.modalStyle = {"display": "block"};
		 
		        $compile(elm)($rootScope);
		      },
		      close: function() {
		        if (elm) {
		          elm.remove();
		        }
		      }
		    };
		 
		    return modal;
		  };
    }
})();


(function() {
    'use strict';

    angular
        .module('app.gstwms')
        .service('parseNumber', parseNumber);
    
    parseNumber.$inject = ['$http','$rootScope'];
    
    function parseNumber($http, $rootScope) {

        function withComma(x) {
          return x;
        }
    }
})();