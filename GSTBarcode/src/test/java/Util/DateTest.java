package Util;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateTest {

	private static final Logger logger =  LoggerFactory.getLogger(DateTest.class);
	
	@Test
	public void test() {
		logger.debug("test start");
		DateTime dt = new DateTime();
		
		String today = dt.getEra() + "" + dt.getYearOfCentury() + "" + dt.getDayOfYear();
		
		System.out.println("centry " + dt.getEra());
		System.out.println(today);
		logger.debug(today);
	}

}
